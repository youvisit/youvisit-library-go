package yvcrypto

import (
	"golang.org/x/crypto/bcrypt"
)

var _ IHasher = &BCrypt{}

// BCrypt : bcrypt hash implements the hasher interface
type BCrypt struct {
	Cost int
}

// Hash : generate hash from plain text
func (b *BCrypt) Hash(plaintText string) (hash string) {
	res, _ := bcrypt.GenerateFromPassword([]byte(plaintText), b.Cost)
	return string(res)
}

// Compare : compare a hash to plaintext
func (b *BCrypt) Compare(hash string, plainText string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(plainText)) == nil
}

// NewBcrypt : get a bcrypt instance that implements the IHasher interface
func NewBcrypt(cost int) IHasher {
	return &BCrypt{
		Cost: cost,
	}
}
