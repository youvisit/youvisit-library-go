package messagebroker

import (
	"encoding/json"
	"os"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/thirdparty/conditional"
	"github.com/aws/aws-lambda-go/events"
	"github.com/gofrs/uuid"
	"github.com/spf13/afero"
)

const (
	FilePathWrite = "/tmp/file-publisher-events"
)

var (
	_                    IMessagePublisher = &FilePublisher{}
	supportedMockTypeSns                   = "SNS"
	supportedMockTypeSqs                   = "SQS"
)

type SnsMockMessage = events.SNSEntity  // SnsMockMessage serializable message
type SqsMockMessage = events.SQSMessage // SqsMockMessage serializable messqge

// FilePublisher
//
//	Description :
//				THIS PUBLISHER IS A PROTOTYPE FOR INTEGRATION TESTING PURPOSES ONLY
// 				IN ORDER TO TEST EVENTS BEING SENT TO APPROPRIATE PLACES
// 				THIS PUBLISHER WILL WRITE FILES TO /tmp/file-publisher-events/uuid.json
// 				YOU WILL NEED A LISTENER LISTENING FOR DIRECTORY
//				CHANGES AND READING THE JSON FILE
//
// Author AHMAD BADREKHAN
//
// Date   17/03/202
type FilePublisher struct {
	fs       afero.Fs
	mockType string
	snsToSQS map[string]string
}

func (f *FilePublisher) generateFilePath(topicName string) string {
	uid, _ := uuid.NewV4()
	return FilePathWrite + "/" + topicName + "_" + uid.String() + ".json"
}

func (f *FilePublisher) writeFile(filePath string, contents []byte) error {
	file, err := f.fs.Create(filePath)
	if err != nil {
		return err
	}
	file.Write([]byte(contents))
	return file.Close()
}

func (f *FilePublisher) PublishMessage(topic string, message string) error {
	return f.PublishMessageWithSubject(topic, message, "")
}

func (f *FilePublisher) PublishMessageWithSubject(topic string, message string, subject string) error {
	var bytes []byte
	var err error

	// depending on mock type create mock message and write bytes
	switch f.mockType {
	case supportedMockTypeSqs:
		bytes, err = json.MarshalIndent(
			&SqsMockMessage{
				EventSourceARN: conditional.String(f.snsToSQS[topic] != "", f.snsToSQS[topic], topic),
				Body:           message,
			},
			"",
			"  ",
		)
		break
	case supportedMockTypeSns:
		bytes, err = json.MarshalIndent(
			&SnsMockMessage{
				TopicArn: topic,
				Subject:  subject,
				Message:  message,
			},
			"",
			"  ",
		)
		break
	}

	if err != nil {
		return err
	}
	return f.writeFile(f.generateFilePath(topic), bytes)
}

func (f *FilePublisher) PublishEvent(topic string, event IEvent) error {
	return f.PublishMessageWithSubject(topic, event.JSON(), "")
}

func (f *FilePublisher) InitTopic(topic string) (topicName string, err error) {
	return topic, nil
}

// NewFileSnsPublisher expects a file system
func NewFileSnsPublisher(fs afero.Fs) *FilePublisher {
	pub := &FilePublisher{
		fs:       fs,
		mockType: supportedMockTypeSns,
	}
	_ = pub.fs.MkdirAll(FilePathWrite, os.ModePerm) // 777 because it's testing purposes
	return pub
}

// NewFileSqsPublisher : expects a file system and an sns to sqs topic mapping if your application
// in the cloud uses sns to publish and sqs to recieve
func NewFileSqsPublisher(fs afero.Fs, snsToSqsTopicMap map[string]string) *FilePublisher {
	if snsToSqsTopicMap == nil {
		snsToSqsTopicMap = make(map[string]string)
	}
	pub := &FilePublisher{
		fs:       fs,
		mockType: supportedMockTypeSqs,
		snsToSQS: snsToSqsTopicMap,
	}
	_ = pub.fs.MkdirAll(FilePathWrite, os.ModePerm) // 777 because it's testing purposes
	return pub
}
