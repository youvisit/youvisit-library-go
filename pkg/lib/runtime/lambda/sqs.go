package lambda

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/messagebroker"
	"github.com/aws/aws-lambda-go/events"
	"log"
	"sync"
)

// SqsConcurrentHandler : sns aws lambda handler if your system has sns messages that need to be done in order, don't use this !
func SqsConcurrentHandler(s *messagebroker.SqsRouterHandler) func(req events.SQSEvent) {
	return func(req events.SQSEvent) {
		var wg sync.WaitGroup
		for _, ev := range req.Records {
			entity := ev
			wg.Add(1)
			// concurrently execute the sns events
			go func() {
				defer wg.Done()
				err := s.Exec(entity)
				log.Print(err) // log the error
			}()
		}
		wg.Wait()
	}
}
