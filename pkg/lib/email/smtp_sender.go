package email

import (
	"fmt"
	"net/smtp"
	"strings"
)

// NewSMTPDetails : new empty object you need to use the builder methods to make a valid connection
func NewSMTPDetails() *SMTPConfig {
	return &SMTPConfig{}
}

// NewSMTPSender : new email client sender that uses smtp ptcl
func NewSMTPSender(sdet *SMTPConfig) *SMTPSender {
	return &SMTPSender{
		details: sdet,
	}
}

// SMTPConfig : server details needed for instantiating an smtp sender
type SMTPConfig struct {
	Host string
	Port int
	Auth smtp.Auth
}

// WithHost : add host
func (s *SMTPConfig) WithHost(h string) *SMTPConfig {
	s.Host = h
	return s
}

// WithPort : add port
func (s *SMTPConfig) WithPort(p int) *SMTPConfig {
	s.Port = p
	return s
}

// WithAuth : add auth scheme
func (s *SMTPConfig) WithAuth(a smtp.Auth) *SMTPConfig {
	s.Auth = a
	return s
}

func (s SMTPConfig) GetDialFormat() string {
	return fmt.Sprintf("%s:%d", s.Host, s.Port)
}

// SMTPSender : email sender that uses the smtp protocol
type SMTPSender struct {
	details *SMTPConfig
}

// SendPlainEmail : send a plain text email , nothing fancy
func (s *SMTPSender) SendPlainEmail(sipt *SendInput, rawBody string) error {
	return smtp.SendMail(
		s.details.GetDialFormat(),                // host:port
		s.details.Auth,                           // auth scheme
		sipt.From,                                // sender
		sipt.GetAllTo(),                          // sending all tos + cc
		[]byte(s.buildMsg(sipt, rawBody, false)), //text/plain msg
	)
}

// SendHTMLEmail : send an HTML formatted email
func (s *SMTPSender) SendHTMLEmail(sipt *SendInput, HTMLBody string) error {
	return smtp.SendMail(
		s.details.GetDialFormat(),                // host:port
		s.details.Auth,                           // auth scheme
		sipt.From,                                // sender
		sipt.GetAllTo(),                          // sending all tos + cc
		[]byte(s.buildMsg(sipt, HTMLBody, true)), //text/HTML msg
	)
}

func (s *SMTPSender) buildMsg(sipt *SendInput, body string, isHTML bool) string {
	var emailType = "plain"
	if isHTML {
		emailType = "html"
	}
	msg := fmt.Sprintf("MIME-version: 1.0;\nContent-Type: text/%s; charset=\"UTF-8\";\r\n", emailType)
	msg += fmt.Sprintf("From: %s\r\n", sipt.From)
	msg += fmt.Sprintf("To: %s\r\n", strings.Join(sipt.GetAllTo(), ";"))
	msg += fmt.Sprintf("Subject: %s\r\n", sipt.Subject)
	msg += fmt.Sprintf("\r\n%s\r\n", body)
	return msg
}
