package dynamo

import (
	"errors"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	"strings"
	"unicode/utf8"
)

func ParseStringToPagingKeyMap(page string) (map[string]string, error) {
	pageKeys := map[string]string{}
	if page != "" {
		keys := strings.Split(page, ",")
		for _, key := range keys {
			tmpKey := strings.Split(key, "=")
			if len(tmpKey) < 2 {
				return nil, errors.New("incorrect format for page field")
			}
			pageKeys[tmpKey[0]] = tmpKey[1]
		}
	}
	return pageKeys, nil
}

func PagingKeyToString(pageKey dynamo.PagingKey) string {
	var page string
	if pageKey != nil {
		for k, v := range pageKey {
			page += k + "=" + *v.S + ","
		}
		page = TrimLastChar(page)
	}
	return page
}

func TrimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}

func BuildPagingKey(pageKeyMap map[string]string) dynamo.PagingKey {
	pKey := dynamo.PagingKey{}
	for key, val := range pageKeyMap {
		attr := dynamodb.AttributeValue{}
		attr.SetS(val)
		pKey[key] = &attr
	}
	return pKey
}
