// Package messagebroker : a package that provides utility for message and sevent sourcing paradigms
// this package will enable you to send events to other systems
//
// Usage :
//		type SomeAService struct {
//			topicForMessage // could be sns  topic arn ....
//			pub messagebroker.IMessagePublisher
//		}
//
//		func (s *SomeAService) doSomething() {
//			// some code ...
//			s.pub.PublishMessage(s.topicForMessage , "some message cool wow !")
//		}
//
//		// through di we can either create a local publisher for testing locally or sns one for testing live
//		// not only that but you can use other message publishers like rabbit mq , kafka ...etc you just need to implement  messagebroker.IMessagePublisher
// 		func NewService(isLocal bool) *SomeAService {
//			var publisher messagebroker.IMessagePublisher
//			if isLocal {
//				publisher = messagebroker.NewSnsPublisher(nil , "someAccount" , "someRegion") //  first arg is  not nil it should be the sns engine ... too lazy to write that code out
//			} else {
//				publisher = messbroker.NewFileSnsPublisher(afero.NewOsFs())
//			}
//			return &SomeAService {
//				pub : publisher
//			}
//		}
//
package messagebroker

//go:generate moq  -out message_publisher_mock.go . IMessagePublisher
// IMessagePublisher : message publisher abstraction that allows us to publish a message / event
type IMessagePublisher interface {
	// SnsMockMessage : publish a message to a message broker to handle
	PublishMessage(topic string, message string) error

	// PublishMessageWithSubject : publish a message with some subject , this subject can be the unique id or something
	PublishMessageWithSubject(topic string, message string, subject string) error

	// PublishMessageWithEvent : publish a message with some event
	PublishEvent(topic string, event IEvent) error

	// InitTopic : initialize the topic if it doesn't exist , useful for migration
	InitTopic(topic string) (topicName string, err error)
}
