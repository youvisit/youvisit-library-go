// Package customerror : basically provides a wrapper around the error type with an addition of status code field
// This Package is Deprecated
//
// this is useful for http routers or bash cli projects
//
// Usage :
//
//	var errorSomethingNotFound = customerror.New(404,"item not found")
//	func foo () customerror.IntlErrors {return errorSomethingNotFound}
//	func consumerOfFoo () {
//		err := foo()
//		if err != nil {
//			fmt.Println(err.StatusCode)
//			fmt.Println(err.ErrorString())
//		}
//	}
package customerror

import (
	"errors"
	"strconv"
	"strings"
)

// IntlErrors : models an internationalized error ,
// ie error suitable for http request thats standardized to give us codes on what's wrong
// we can reuse this even if it's internal logic , as the http status codes usually cover most of our needs
type IntlErrors struct {
	StatusCode int `json:"statusCode"`
	Error      error
}

// New : generate a new instance of the IntlErrors
// example :
//
//	customerror.New(http.StatusBadRequest , "missing id in the field")
func New(statusCode int, errorMessage string) *IntlErrors {
	return &IntlErrors{
		StatusCode: statusCode,
		Error:      errors.New(errorMessage),
	}
}

// FromError : take a standard error and convert it to an IntlError
//
// Note the format expected is of the form :
//
//	i.FromError(errors.New("400,Could not find this object")) => "statuscode,errorMessage"
func FromError(err error) *IntlErrors {
	message := err.Error()
	messageAr := strings.Split(message, ",")
	message = messageAr[0]
	messageCode, errAtoi := strconv.Atoi(message)
	if errAtoi != nil {
		return nil
	}
	return &IntlErrors{
		StatusCode: messageCode,
		Error:      errors.New(strings.TrimSpace(messageAr[1])),
	}
}

// ErrorString : ouput the error as string
func (i *IntlErrors) ErrorString() string {
	return i.Error.Error()
}
