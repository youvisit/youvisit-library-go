package http

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
)

// this error is implemented
var _ error = &RequestError{}

// RequestError : http request specific error that implements the error interface
type RequestError struct {
	StatusCode int
	Err        error
}

func (r *RequestError) Error() string {
	return r.Err.Error()
}

// NewReqErr : regular construct
func NewReqErr(statusCode int, errString string) *RequestError {
	return &RequestError{
		StatusCode: statusCode,
		Err:        errors.New(errString),
	}
}

// NewReqErrFromSTDError : new error from error
func NewReqErrFromSTDError(statusCode int, err error) *RequestError {
	return &RequestError{
		StatusCode: statusCode,
		Err:        err,
	}
}

// ParseReqErrfromSTDErr : parse a request from standard errors with the following format
// <<status_code>>,...errormsg => 400,this is a bad request
func ParseReqErrfromSTDErr(err error) *RequestError {
	message := err.Error()
	messageAr := strings.Split(message, ",")
	message = messageAr[0]
	messageCode, errAtoi := strconv.Atoi(message)
	if errAtoi != nil {
		return nil
	}

	return &RequestError{
		StatusCode: messageCode,
		Err:        errors.New(strings.TrimSpace(messageAr[1])),
	}
}

// NewReqErrFromIntlErr : convert *customerror.IntlErrors to an error
func NewReqErrFromIntlErr(err *customerror.IntlErrors) *RequestError {
	if err == nil {
		return nil
	}
	return &RequestError{
		StatusCode: err.StatusCode,
		Err:        err.Error,
	}
}

// NewComposeErrReq : comspose an error if you need some user input for your format
// this is useful for errors you want to store the format and call with arguments later on
func NewComposeErrReq(statusCode int, errFmt string) func(a ...interface{}) *RequestError {
	return func(a ...interface{}) *RequestError {
		return &RequestError{
			StatusCode: statusCode,
			Err:        fmt.Errorf(errFmt, a...),
		}
	}
}
