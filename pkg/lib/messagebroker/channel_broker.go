package messagebroker

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync/atomic"

	"github.com/aws/aws-lambda-go/events"
	"go.uber.org/zap"
)

var (
	_              IMessagePublisher = &ChannelBroker{}
	AUTO_INCREMENT int32             // i'm using underscores as it's familiar to mysql people
)

type ChannelMessage struct {
	Topic   string
	Data    []byte
	Subject string
	ID      string
}

func (c *ChannelMessage) ToSQSMessage() *events.SQSMessage {

	var mp map[string]interface{} = make(map[string]interface{})
	mp["Message"] = string(c.Data)
	dt, err := json.Marshal(&mp)
	if err != nil {
		log.Fatal("could not convert message")
	}
	return &events.SQSMessage{
		Body:           string(dt),
		EventSourceARN: c.Topic,
		MessageId:      c.ID,
	}
}

func (c *ChannelMessage) ToString() string {
	var sb strings.Builder
	sb.Write([]byte(fmt.Sprintf("\n%s: %v\n", "id", c.ID)))
	sb.Write([]byte(fmt.Sprintf("%s: %v\n", "topic", c.Topic)))
	sb.Write([]byte(fmt.Sprintf("%s: %v\n", "subject", c.Subject)))
	return sb.String()
}

func NewDefaultChannelBroker() *ChannelBroker {
	return &ChannelBroker{
		logger: zap.NewNop(),
	}
}

func NewChannelBroker(log *zap.Logger) *ChannelBroker {
	return &ChannelBroker{
		logger: log,
	}
}

type ChannelBroker struct {
	logger       *zap.Logger
	EventChannel chan ChannelMessage
	DlqChannel   chan ChannelMessage
}

func (c *ChannelBroker) ListenFromSQSRouter(router *SqsRouterHandler) {
Free:
	for {
		msg, ok := <-c.EventChannel
		if !ok {
			c.logger.Info("Channel got closed...")
			// channel got closed
			break Free
		}
		go func() {
			lger := c.logger.Named(msg.Topic).Named(msg.ID)
			lger.Sugar().Infof("Recieved a message %s", msg.ToString())
			err := router.Exec(*msg.ToSQSMessage(), true)
			if err != nil {
				lger.Sugar().Infof("Errored out ! because of (%s) moving to DLQ Channel %s", err.Error(), router.GetDlqTopic(msg.Topic))
				c.DlqChannel <- ChannelMessage{
					Topic:   router.GetDlqTopic(msg.Topic),
					Data:    msg.Data,
					Subject: msg.Subject,
					ID:      msg.ID,
				}
			}
			lger.Sugar().Info("OK! no Queue Error")
		}()
	}
}

// SnsMockMessage : publish a message to a message broker to handle
func (c *ChannelBroker) PublishMessage(topic string, message string) error {
	return c.PublishMessageWithSubject(topic, message, "")
}

// PublishMessageWithSubject : publish a message with some subject , this subject can be the unique id or something
func (c *ChannelBroker) PublishMessageWithSubject(topic string, message string, subject string) error {
	id := atomic.AddInt32(&AUTO_INCREMENT, 1)
	lger := c.logger.Named(topic).Named(fmt.Sprintf("%d", id))
	lger.Info("publishing message to event channel ...")
	c.EventChannel <- ChannelMessage{
		Topic:   topic,
		Data:    []byte(message),
		Subject: subject,
		ID:      fmt.Sprintf("%d", id),
	}
	return nil
}

// PublishMessageWithEvent : publish a message with some event
func (c *ChannelBroker) PublishEvent(topic string, event IEvent) error {
	return c.PublishMessageWithSubject(topic, event.JSON(), "")
}

// InitTopic : initialize the topic if it doesn't exist , useful for migration
func (c *ChannelBroker) InitTopic(topic string) (topicName string, err error) {
	c.logger.Named("topic=" + topic).Info("This topic does not need init logic")
	return topic, nil
}
