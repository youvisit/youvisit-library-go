// Package http : package that provides abstract http router engine.
//
// this allows us to easily switch to different http routers , gin fiber ..etc
// the IContext interface contains standard methods most http routers supports
// furthermore it adds status code returns
//
// Usage :
//
//	// https://somewebsite.com/students/:id
//	func getStudentIdAndPrint(c http.IContext) {
//		studentId := c.Param("id")
//		c.Ok(studentId) // supports ok , created , updated , deleted ...etc depedning on what happened it will return correct status code
//	}
//
//	// if you're using the gin implementation
//	g := gin.Default()
//	g.Get("students/:id",http.HandleGin(getStudentIdAndPrint))
package http

import (
	"io"
	"net/http"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
)

const (
	StatusContinue           = 100 // RFC 9110, 15.2.1
	StatusSwitchingProtocols = 101 // RFC 9110, 15.2.2
	StatusProcessing         = 102 // RFC 2518, 10.1
	StatusEarlyHints         = 103 // RFC 8297

	StatusOK                   = 200 // RFC 9110, 15.3.1
	StatusCreated              = 201 // RFC 9110, 15.3.2
	StatusAccepted             = 202 // RFC 9110, 15.3.3
	StatusNonAuthoritativeInfo = 203 // RFC 9110, 15.3.4
	StatusNoContent            = 204 // RFC 9110, 15.3.5
	StatusResetContent         = 205 // RFC 9110, 15.3.6
	StatusPartialContent       = 206 // RFC 9110, 15.3.7
	StatusMultiStatus          = 207 // RFC 4918, 11.1
	StatusAlreadyReported      = 208 // RFC 5842, 7.1
	StatusIMUsed               = 226 // RFC 3229, 10.4.1

	StatusMultipleChoices  = 300 // RFC 9110, 15.4.1
	StatusMovedPermanently = 301 // RFC 9110, 15.4.2
	StatusFound            = 302 // RFC 9110, 15.4.3
	StatusSeeOther         = 303 // RFC 9110, 15.4.4
	StatusNotModified      = 304 // RFC 9110, 15.4.5
	StatusUseProxy         = 305 // RFC 9110, 15.4.6

	StatusTemporaryRedirect = 307 // RFC 9110, 15.4.8
	StatusPermanentRedirect = 308 // RFC 9110, 15.4.9

	StatusBadRequest                   = 400 // RFC 9110, 15.5.1
	StatusUnauthorized                 = 401 // RFC 9110, 15.5.2
	StatusPaymentRequired              = 402 // RFC 9110, 15.5.3
	StatusForbidden                    = 403 // RFC 9110, 15.5.4
	StatusNotFound                     = 404 // RFC 9110, 15.5.5
	StatusMethodNotAllowed             = 405 // RFC 9110, 15.5.6
	StatusNotAcceptable                = 406 // RFC 9110, 15.5.7
	StatusProxyAuthRequired            = 407 // RFC 9110, 15.5.8
	StatusRequestTimeout               = 408 // RFC 9110, 15.5.9
	StatusConflict                     = 409 // RFC 9110, 15.5.10
	StatusGone                         = 410 // RFC 9110, 15.5.11
	StatusLengthRequired               = 411 // RFC 9110, 15.5.12
	StatusPreconditionFailed           = 412 // RFC 9110, 15.5.13
	StatusRequestEntityTooLarge        = 413 // RFC 9110, 15.5.14
	StatusRequestURITooLong            = 414 // RFC 9110, 15.5.15
	StatusUnsupportedMediaType         = 415 // RFC 9110, 15.5.16
	StatusRequestedRangeNotSatisfiable = 416 // RFC 9110, 15.5.17
	StatusExpectationFailed            = 417 // RFC 9110, 15.5.18
	StatusTeapot                       = 418 // RFC 9110, 15.5.19 (Unused)
	StatusMisdirectedRequest           = 421 // RFC 9110, 15.5.20
	StatusUnprocessableEntity          = 422 // RFC 9110, 15.5.21
	StatusLocked                       = 423 // RFC 4918, 11.3
	StatusFailedDependency             = 424 // RFC 4918, 11.4
	StatusTooEarly                     = 425 // RFC 8470, 5.2.
	StatusUpgradeRequired              = 426 // RFC 9110, 15.5.22
	StatusPreconditionRequired         = 428 // RFC 6585, 3
	StatusTooManyRequests              = 429 // RFC 6585, 4
	StatusRequestHeaderFieldsTooLarge  = 431 // RFC 6585, 5
	StatusUnavailableForLegalReasons   = 451 // RFC 7725, 3

	StatusInternalServerError           = 500 // RFC 9110, 15.6.1
	StatusNotImplemented                = 501 // RFC 9110, 15.6.2
	StatusBadGateway                    = 502 // RFC 9110, 15.6.3
	StatusServiceUnavailable            = 503 // RFC 9110, 15.6.4
	StatusGatewayTimeout                = 504 // RFC 9110, 15.6.5
	StatusHTTPVersionNotSupported       = 505 // RFC 9110, 15.6.6
	StatusVariantAlsoNegotiates         = 506 // RFC 2295, 8.1
	StatusInsufficientStorage           = 507 // RFC 4918, 11.5
	StatusLoopDetected                  = 508 // RFC 5842, 7.2
	StatusNotExtended                   = 510 // RFC 2774, 7
	StatusNetworkAuthenticationRequired = 511 // RFC 6585, 6
)

var (
	errorToStatusCodeMap map[error]*int
	responseFuncToRun    ModifyResponseFunc
)

type ModifyResponseFunc func(ctx IContext, res interface{}) interface{}

func IntPtr(i int) *int {
	return &i
}

// OnSuccessfulResponse : modify a successful response useful when you want to omit data or add additional data
// This will have the object before it gets wrapped with {data:<your_interface>,message:""} it will only have <your_interface>
func OnSuccessfulResponse(f ModifyResponseFunc) {
	responseFuncToRun = f
}

func modifyResponse(ctx IContext, res interface{}) interface{} {
	if responseFuncToRun != nil && res != nil {
		res = responseFuncToRun(ctx, res)
		return res
	}
	return res
}

// SetErrorHandlingList : fixed error handling list of items
//
// this is not thread safe , so do not be using this everywhere
// this function is meant to be used once
func SetErrorHandlingList(errorToStatusCode map[error]*int) {
	errorToStatusCodeMap = errorToStatusCode
}

// Response : our base response entity
type Response struct {
	Resource interface{} `json:"data"`
	Message  string      `json:"message"`
}

// ResponseV2 : our base response entity
type ResponseV2 struct {
	Resource interface{} `json:"resource"`
	Message  string      `json:"message"`
}

// BatchedResponseV2 : use this for an update all / delete all operation
type BatchedResponseV2 struct {
	ResponseV2
	RowsModified *int64 `json:"rowsModified"`
}

// BatchedResponse : use this for an updateall / delete all operation
type BatchedResponse struct {
	Response
	RowsModified *int64 `json:"rows_modified"`
}

// BulkReadResponse : use this when we serialize the data into csv and read all
type BulkReadResponse struct {
	Response
	RowsDumped *int64 `json:"rows_dumped"`
}

// BulkReadResponseV2 : use this when we serialize the data into csv and read all
type BulkReadResponseV2 struct {
	ResponseV2
	RowsDumped *int64 `json:"rowsDumped"`
}

// Cookie : cookie requirements
// defaults are / for path
// and samesite = strict
type Cookie struct {
	Name     string
	Value    string
	Duration int
	Path     string
	Domain   string
	Secure   bool
	HTTPOnly bool
	SameSite http.SameSite
}

// WithName : provide name of cookie
func (c *Cookie) WithName(n string) *Cookie {
	c.Name = n
	return c
}

// WithValue : encode cookie value
func (c *Cookie) WithValue(v string) *Cookie {
	c.Value = v
	return c
}

// WithDuration : provide cookie duration as seconds
func (c *Cookie) WithDuration(d int) *Cookie {
	c.Duration = d
	return c
}

// WithPath : enforce sub-path , default is `/`
func (c *Cookie) WithPath(p string) *Cookie {
	c.Path = p
	return c
}

// WithDomain : add domain
func (c *Cookie) WithDomain(d string) *Cookie {
	c.Domain = d
	return c
}

// WithSecure : send only over https
func (c *Cookie) WithSecure() *Cookie {
	c.Secure = true
	return c
}

// WithHTTPOnly : set a flag to tell the client that
// only browser has access to this cookie , js has no access
func (c *Cookie) WithHTTPOnly() *Cookie {
	c.HTTPOnly = true
	return c
}

func (c *Cookie) WithSameSite(samSite http.SameSite) *Cookie {
	c.SameSite = samSite
	return c
}

// BuildCookie : cookie builder
func BuildCookie() *Cookie {
	return &Cookie{
		Path:     "/",
		SameSite: http.SameSiteStrictMode,
	}
}

// IContext : abstract http request object that most engines would pass
// this request interface supports parsing the raw request to an object
// as well as providiing status code returns
//
// This makes testing http routes very easily since this is an interface and all you would have to do is use the http.IContextMock
// Struct that implements the interface
//
//go:generate moq  -out context_mock.go . IContext
type IContext interface {
	// AbortWithStatusFromErrorBadRequest : aborts with status 400 , bad request and uses error string for the message
	AbortWithStatusFromErrorBadRequest(err error)
	// AbortWithStatusFromIntlError : uses the intl errors http status code in the struct and the error string for the message
	AbortWithStatusFromIntlError(err *customerror.IntlErrors)
	// AbortWithError : if you enroll the errors using the global func , this will allow you to handle them here
	AbortWithError(err error)
	// Ok : status 200 , ok
	Ok(res interface{})
	SetHeader(k string, v string)
	// Redirect : redirect With 302
	Redirect(url string)
	// RedirectWStatus : redirect with status code custom
	RedirectWStatus(url string, statusCode int)
	// Accepted : status 202 , ok processing ...
	Accepted(res interface{})
	// OkManyCSV : returned multiple back , status 200 , with rowsDumped equal to the amount
	OkManyCSV(res interface{}, rowsInCSV int64)
	// Created : status 201 , created record
	Created(res interface{})
	// CreatedMany : status 201 , created records with count number
	CreatedMany(res interface{}, rows int64)
	// Updated : status 200 , updated record
	Updated(res interface{})
	// UpdatedMany : status 200 , many records with count number
	UpdatedMany(res interface{}, rows int64)
	// Deleted : status 200 , deleted record
	Deleted(res interface{})
	// DeletedMany many : deleted many records with count number
	DeletedMany(res interface{}, rows int64)
	// BindJSONBodyWithExit : this method will set header of 400 if it can't bind body , you use the boolean to either exit the function or not
	BindJSONBodyWithExit(body interface{}) bool
	// BindQueryWithExit : this method will set header of 400 if it can't bind query param and return false , you use the boolean to either exit the function or not
	BindQueryWithExit(query interface{}) bool
	// IdParam : get param from uri with id label
	IdParam() string
	// Param : get a specific param
	Param(key string) string
	// Next : go next
	Next()
	// GetHeader : get a header value given a key
	GetHeader(key string) string
	// GetRawRequest : get underlying http raw request
	GetRawRequest() *http.Request
	GetQueryParam(key string) string
	// SendContext : use this to send context to the next handler function in the chain
	// a good use case is to send account context to the next handler , ie your controller
	SendContext(key string, data interface{})
	// GetContext : use this to get the context you had sent from your middleware to your controller.
	// pass the dataBinding as pointer for the struct you want to have the value populated
	//
	// the function returns a boolean if the context for the key was found
	//
	// NB : => if you pass the wrong struct for the key , then it will panic .
	// This was made by design
	// so as a dev you pay attention
	GetContext(key string, dataBinding interface{}) (isFound bool)
	// GetCookie : grabs cookie from client
	GetCookie(cookieName string) (val string, isFound bool)
	// SetCookie : set cookie with a specified value
	// use the cookie builder for an easier time building cookies
	SetCookie(co *Cookie)
	// DeleteCookie : delete a cookie by nullifying it on the client side
	DeleteCookie(cookieName, domain string)
	// OutputFile : output file to client
	OutputFile(file io.Reader, fileName string)
}
