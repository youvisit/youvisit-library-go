// Package local : provides a runtime listener for sns and sqs messages if you use a local file publisher under the messagebroker package
package local

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/messagebroker"
	"github.com/aws/aws-lambda-go/events"
	"github.com/davecgh/go-spew/spew"
	"github.com/pterm/pterm"
	"github.com/radovskyb/watcher"
)

// RunSnsLocalFileWatcher run sns watcher based on FilePublisher spec
// it will watch for file changes under the tmp folder and wait for a json event
func RunSnsLocalFileWatcher(s *messagebroker.SnsRouterHandler) {
	w := watcher.New()

	// unlimited events
	w.SetMaxEvents(-1)

	// Only notify create events
	w.FilterOps(watcher.Create, watcher.Write)

	go func() {
		for {
			select {
			case event := <-w.Event:
				pterm.Info.Println("got an event")
				b, err := os.ReadFile(event.Path)
				if err != nil {
					pterm.Warning.Println(err)
					break
				}
				var ev events.SNSEntity
				err = json.Unmarshal(b, &ev)
				if err != nil {
					pterm.Warning.Println(err)
					break
				}
				pterm.Info.Println(fmt.Sprintf("EXECUTING => %s", ev.TopicArn))
				err = s.Exec(ev)
				if err != nil {
					pterm.Error.Println(err)
					break
				}
				pterm.Success.Println("EXECUTED EVENT SUCCESSFULLY")
			case err := <-w.Error:
				pterm.Error.Println(err)
				os.Exit(1)
			case <-w.Closed:
				spew.Dump("closed")
				return
			}
		}
	}()

	// Watch the FilePublisher path recursively
	if err := w.AddRecursive(messagebroker.FilePathWrite); err != nil {
		log.Fatalln(err)
	}

	pterm.Info.Println(fmt.Sprintf("LISTENING FOR EVENTS ... on folder %s", messagebroker.FilePathWrite))
	// Start the watching process - it'll check for changes every 100ms.
	if err := w.Start(time.Millisecond * 100); err != nil {
		log.Fatalln(err)
	}

}
