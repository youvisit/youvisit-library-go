package cloud

import (
	"sync"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/messagebroker"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"go.uber.org/zap"
)

var (
	// MaxMessages : Maximum messages per call (default is 2)
	MaxMessages int64 = 2
	// WaitTimeSeconds : Wait time before next msg
	WaitTimeSeconds int64 = 15
	// DefaultRegion : default region
	DefaultRegion = "us-east-1"
	// DefaultDeleteMode : default mode for deleting
	DefaultDeleteMode = DeleteModeLax
	// accountID : aws acc id
	accountID string
	// IsConcurrent : concurrently run the events
	IsConcurrent bool = false
	// mutex lock
	mu sync.Mutex
	// topicToQueueURLMap : map for names of queues
	topicToQueueURLMap    map[string]string = map[string]string{}
	dlqTopicToQueueURLMap map[string]string = make(map[string]string)
	// VisibilityTimeoutSeconds : timeout before a message gets sent back to the queue
	VisibilityTimeoutSeconds int64 = 60

	logger *zap.Logger
)

type DeleteMode int

const (
	DeleteModeLax DeleteMode = iota
	DeleteModeEager
)

// SetVisbilityTimeout : override the default visibilty timeout
func SetVisbilityTimeout(s int64) {
	VisibilityTimeoutSeconds = s
}

// SetMaxSQSMessages : max messsages you want
func SetMaxSQSMessages(msgCount int64) {
	MaxMessages = msgCount
}

func SetLogger(l *zap.Logger) {
	logger = l
}

// SetSQSWaitTimeSeconds : set wait time for message batch
func SetSQSWaitTimeSeconds(waitTimeS int64) {
	WaitTimeSeconds = waitTimeS
}

// SetSQSIsConcurrent : is concurrent execution of events
func SetSQSIsConcurrent(isConcurrent bool) {
	IsConcurrent = isConcurrent
}

// SetSQSDefaultRegion : set default aws region
func SetSQSDefaultRegion(region string) {
	DefaultRegion = region
}

// SetDeleteMode : allows you to set the delete mode as either lax or eager
//
// lax mode => deletes after your handler runs the message logic
// eager mode => deletes as soon as message is recieved
func SetDeleteMode(mode DeleteMode) {
	DefaultDeleteMode = mode
}

// SQSMessageChan : message from sqs for channels
type SQSMessageChan struct {
	ReceiptHandler *string
	QUrl           *string
	topic          string
	dlqTopic       string
	dlqQUrl        *string
	msg            *sqs.Message
}

func pollSQSMessages(driver sqsiface.SQSAPI, topic string, dlqTopic string, messages []*SQSMessageChan) []*SQSMessageChan {
	qUrl := aws.String("")
	dlqUrl := aws.String("")
	if topicToQueueURLMap[topic] == "" {
		out, err := driver.GetQueueUrl(&sqs.GetQueueUrlInput{
			QueueName:              aws.String(topic),
			QueueOwnerAWSAccountId: aws.String(accountID),
		})
		if err != nil {
			logger.Sugar().Info("failed to fetch sqs message", err)
			return messages
		}
		qUrl = out.QueueUrl
		if dlqTopic != "" {
			out2, err := driver.GetQueueUrl(&sqs.GetQueueUrlInput{
				QueueName:              aws.String(dlqTopic),
				QueueOwnerAWSAccountId: aws.String(accountID),
			})
			if err == nil {
				dlqUrl = out2.QueueUrl
			}
		}
		mu.Lock()
		topicToQueueURLMap[topic] = *qUrl
		topicToQueueURLMap[dlqTopic] = *dlqUrl
		mu.Unlock()

	} else {
		qUrl = aws.String(topicToQueueURLMap[topic])
		if dlqTopic != "" {
			dlqUrl = aws.String(topicToQueueURLMap[dlqTopic])
		}
	}
	output, err := driver.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl:            qUrl,
		MaxNumberOfMessages: aws.Int64(MaxMessages),
		WaitTimeSeconds:     aws.Int64(WaitTimeSeconds),
		VisibilityTimeout:   aws.Int64(VisibilityTimeoutSeconds),
	})
	if err != nil {
		logger.Sugar().Info("failed to recieve sqs message", err)
		return messages
	}

	for _, message := range output.Messages {
		messages = append(messages, &SQSMessageChan{
			ReceiptHandler: message.ReceiptHandle,
			QUrl:           qUrl,
			msg:            message,
			topic:          topic,
			dlqTopic:       dlqTopic,
			dlqQUrl:        dlqUrl,
		})
	}
	return messages

}

func DelMsg(driver sqsiface.SQSAPI, qurl *string, receiptHandler *string, logger *zap.Logger) {
	_, err := driver.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      qurl,
		ReceiptHandle: receiptHandler,
	})
	if err != nil {
		logger.Sugar().Infof("failed to delete message due to %w ", err.Error())
	}
}

func handleSQSMessage(driver sqsiface.SQSAPI, handler *messagebroker.SqsRouterHandler, msg *SQSMessageChan) {

	logger.Sugar().Info("handling message for ", msg.topic, msg.msg.Body)
	if DeleteModeEager == DefaultDeleteMode {
		logger.Sugar().Info("eagerly deleting message %s", msg.topic)
		DelMsg(driver, msg.QUrl, msg.ReceiptHandler, logger)
	}
	err := handler.Exec(events.SQSMessage{
		EventSourceARN: msg.topic,
		Body:           *msg.msg.Body,
	}, true)
	if err != nil {
		logger.Sugar().Infof("failed to execute topic %s %w", msg.topic, err)
		if msg.dlqQUrl != nil && *msg.dlqQUrl != "" {
			logger.Sugar().Infof("sending message to the dlq %s with url %s", msg.dlqTopic, *msg.dlqQUrl)
			_, err := driver.SendMessage(&sqs.SendMessageInput{
				QueueUrl:    msg.dlqQUrl,
				MessageBody: msg.msg.Body,
			})
			if err != nil {
				logger.Sugar().Infof("failed sending message to the dlq %w", err)
			}
		} else {
			logger.Sugar().Infof("no dlq found for this topic %s ... skipping", msg.topic)
		}

	}
	if DeleteModeLax == DefaultDeleteMode {
		logger.Sugar().Info("lax deleting message %s after processing is done", msg.topic)
		DelMsg(driver, msg.QUrl, msg.ReceiptHandler, logger)
	}
}

// RunSQSWatcher : run sqs message watcher , it can handle messages concurrently or single
func RunSQSWatcher(AWSAccountID string, driver sqsiface.SQSAPI, s *messagebroker.SqsRouterHandler) {
	if logger == nil {
		logger = zap.NewNop()
	}
	logger.Sugar().Infof("Your SQS Poller Has, visibility_timeout=%d,wait_time_seconds=%d", VisibilityTimeoutSeconds, WaitTimeSeconds)
	accountID = AWSAccountID
	for {
		var wg sync.WaitGroup
		for _, topic := range s.GetAllTopics() {
			wg.Add(1)
			go func(topic string) {
				defer wg.Done()
				var messages []*SQSMessageChan = []*SQSMessageChan{}
				messages = pollSQSMessages(driver, topic, s.GetDlqTopic(topic), messages)
				for _, message := range messages {
					handleSQSMessage(driver, s, message)
				}
			}(topic)
		}
		wg.Wait()
	}

}
