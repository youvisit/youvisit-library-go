// Package email : abstract email interface that wraps around any mail implementation .
// Such as SES , ...etc . Add to the interface !
package email

// SendInput : Sender input , the fields are self-explanatory
type SendInput struct {
	From    string
	To      string
	CC      []string
	Subject string
}

// GetAllTo : gives back a slice of the to and the cc as 1 slice
func (s *SendInput) GetAllTo() []string {
	slc := make([]string, 0, len(s.CC)+1)
	slc = append(slc, s.To)
	slc = append(slc, s.CC...)
	return slc
}

//go:generate moq -skip-ensure -out sender_mock.go . ISender

// ISender : Sender Interface
type ISender interface {
	// SendPlainEmail : send a plain email
	SendPlainEmail(s *SendInput, rawBody string) error
	// SendHTMLEmail : send an HTML formatted email
	//                 When Sending HTML Emails the limitations are
	//                 inherited by  the implementer  of this interface
	//
	// Example : If you're using an SES sender (which implements this interface)
	//           and if that underlying service does not support javascript
	//           then calling this method  will still inherit that limitation
	SendHTMLEmail(s *SendInput, HTMLBody string) error
}
