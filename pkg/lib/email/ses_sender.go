package email

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/thirdparty/conditional"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/aws/aws-sdk-go/service/ses/sesiface"
	"github.com/pkg/errors"
)

var _ ISender = &SESSender{}

const (
	SESSenderErrorPrefix = "SES_SENDER_AWS_ERROR"
)

// SESSender : uses the aws ses service to email a client
type SESSender struct {
	driver sesiface.SESAPI
	emailEncoding string
}


func (S *SESSender) generateSendEmailInput(s *SendInput,body string,isHTML bool) *ses.SendEmailInput {
	var awsBody ses.Body
	if isHTML {
		awsBody = ses.Body{
			Html:&ses.Content {
				Charset: aws.String(S.emailEncoding),
				Data:    aws.String(body),
			},
		}
	} else {
		awsBody = ses.Body{
			Text:&ses.Content {
				Charset: aws.String(S.emailEncoding),
				Data:    aws.String(body),
			},
		}
	}
	return &ses.SendEmailInput{
		Destination: &ses.Destination{
			CcAddresses: S.ccToAWS(s.CC),
			ToAddresses: []*string{
				aws.String(s.To),
			},
		},
		Message: &ses.Message{
			Body: &awsBody,
			Subject: &ses.Content{
				Charset: aws.String(S.emailEncoding),
				Data:    aws.String(s.Subject),
			},
		},
		Source: aws.String(s.From),
	}
}


func (S *SESSender) ccToAWS(ccs []string ) []*string {
	var ccAws []*string
	for _,cc := range ccs {
		ccAws = append(ccAws,aws.String(cc))
	}
	return ccAws
}

// SendPlainEmail : send a plain text email , nothing fancy
//
//
// PS : if you're going to send plain email internally
//     might as well use sns and have a subscription to an email
//     it's simpler
func (S *SESSender) SendPlainEmail(s *SendInput, rawBody string) error {
	_,err := S.driver.SendEmail(S.generateSendEmailInput(s,rawBody,false))
	if err != nil {
		errors.Wrap(err,SESSenderErrorPrefix)
	}
	return nil
}


// SendHTMLEmail : send an HTML formatted email , HTML limitations will be inherited from ses
//                 so refer to their documentation in regard to what you can and can't do
func (S *SESSender) SendHTMLEmail(s *SendInput, HTMLBody string) error {
	_,err := S.driver.SendEmail(S.generateSendEmailInput(s,HTMLBody,true))
	if err != nil {
		errors.Wrap(err,SESSenderErrorPrefix)
	}
	return nil
}

// NewSESSender : new email sender interface using ses as the driver
func NewSESSender(driver sesiface.SESAPI,emailEncoding string) ISender {
	return &SESSender{
		driver:        driver,
		emailEncoding: conditional.String(emailEncoding == "","",emailEncoding),
	}
}
