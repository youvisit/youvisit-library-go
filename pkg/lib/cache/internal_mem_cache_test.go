package cache

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_cache_InternalMemCache_Get(t *testing.T) {
	// test string fetch
	{
		items := make(map[string]Item)
		var res string
		items["ahmad"] = Item{
			resource:   "hello",
			expiryTime: time.Time{},
		}
		cache := InternalMemCache{items: items}

		isFound := cache.Get("ahmad", &res)
		assert.True(t, isFound)
		assert.Equal(t, "hello", res)
	}
	// test struct fetch
	{
		type someStruct struct {
			name string
		}
		items := make(map[string]Item)
		var res someStruct
		items["ahmad"] = Item{resource: someStruct{name: "ahmad"}, expiryTime: time.Time{}}
		cache := InternalMemCache{items: items}

		isFound := cache.Get("ahmad", &res)
		assert.True(t, isFound)
		assert.Equal(t, "ahmad", res.name)
	}
	// test numb fetch
	{

		items := make(map[string]Item)
		var res int
		items["ahmad"] = Item{
			resource:   1,
			expiryTime: time.Time{},
		}
		cache := InternalMemCache{items: items}

		isFound := cache.Get("ahmad", &res)
		assert.True(t, isFound)
		assert.Equal(t, 1, res)
	}
	// test expired thing
	{
		expiredTime := time.Now().Unix() - 100

		items := make(map[string]Item)
		var res int
		items["ahmad"] = Item{
			resource:   1,
			expiryTime: time.Unix(expiredTime, 0),
		}
		cache := InternalMemCache{items: items}
		isFound := cache.Get("ahmad", &res)
		assert.False(t, isFound)
		assert.Equal(t, 0, res)
		assert.Len(t, cache.items, 0)
	}

	// test non expired thing
	{
		expiredTime := time.Now().Unix() + 100

		items := make(map[string]Item)
		var res int
		items["ahmad"] = Item{
			resource:   1,
			expiryTime: time.Unix(expiredTime, 0),
		}
		cache := InternalMemCache{items: items}
		isFound := cache.Get("ahmad", &res)
		assert.True(t, isFound)
		assert.Equal(t, 1, res)
		assert.Len(t, cache.items, 1)
	}
}

func Test_cache_InternalMemCache_Set(t *testing.T) {
	items := make(map[string]Item)
	cache := InternalMemCache{items: items}
	cache.Set("ahmad", "1234")
	assert.Equal(t, "1234", items["ahmad"].resource)
	assert.True(t, cache.items["ahmad"].expiryTime.IsZero())
}

func Test_cache_InternalMemCache_SetWithExpire(t *testing.T) {
	var res string
	items := make(map[string]Item)
	cache := InternalMemCache{items: items}
	cache.SetWithExpire("ahmad", "1234", 1)
	assert.Equal(t, "1234", items["ahmad"].resource)
	assert.False(t, cache.items["ahmad"].expiryTime.IsZero())
	time.Sleep(2 * time.Second)
	isFound := cache.Get("ahmad", &res) // we can no longer get if expired
	assert.False(t, isFound)
}

func Test_cache_InternalMemCache_Delete(t *testing.T) {
	var res string
	items := make(map[string]Item)
	cache := InternalMemCache{items: items}
	cache.Set("ahmad", "1234")
	assert.Equal(t, "1234", items["ahmad"].resource)
	assert.True(t, cache.items["ahmad"].expiryTime.IsZero())
	cache.Delete("ahmad")
	assert.Len(t, cache.items, 0)
	assert.False(t, cache.Get("ahmad", &res))
}

func Test_cache_InternalMemCache_Flush(t *testing.T) {

	items := make(map[string]Item)
	cache := InternalMemCache{items: items}

	// init
	cache.Set("ahmad", "1234")
	cache.Set("somethingElse", "12424")
	cache.Set("someOthjerThing", 1424)
	cache.Set("someElse", false)

	assert.Len(t, cache.items, 4)

	cache.Flush()

	assert.Len(t, cache.items, 0)
}
