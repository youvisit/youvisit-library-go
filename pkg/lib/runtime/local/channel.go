package local

import (
	"fmt"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/messagebroker"
)

func RunEventChannelListener(broker *messagebroker.ChannelBroker, s *messagebroker.SqsRouterHandler) {
	fmt.Printf("Listening for topics with %v", s.GetAllTopics())
	broker.ListenFromSQSRouter(s)
}
