package messagebroker

import (
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

var _ IMessagePublisher = &SQSPublisher{}

// ISqs : sqs api
type ISqs = sqsiface.SQSAPI

// SQSPublisher : implements the message broker with sqs as a message publisher
type SQSPublisher struct {
	driver       ISqs
	awsAccountID string
	region       string
	queueCache   map[string]string
	mu           sync.Mutex
}

// getQueueURL : fetches the queue url name
func (s *SQSPublisher) getQueueURL(topicName string) (*string, error) {
	cache := s.queueCache[topicName]
	if cache != "" {
		return &cache, nil
	}
	out, err := s.driver.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName:              aws.String(topicName),
		QueueOwnerAWSAccountId: aws.String(s.awsAccountID),
	})
	if err != nil {
		return nil, err
	}
	s.mu.Lock()
	s.queueCache[topicName] = *out.QueueUrl
	s.mu.Unlock()
	return out.QueueUrl, nil
}

// PublishMessage : publishes a messaeg without a subject
func (s *SQSPublisher) PublishMessage(topic string, message string) error {
	q, err := s.getQueueURL(topic)
	if err != nil {
		return err
	}
	_, err = s.driver.SendMessage(&sqs.SendMessageInput{
		MessageBody: &message,
		QueueUrl:    q,
	})
	return err
}

// PublishMessageWithSubject : publishes a message with the subject in the header
func (s *SQSPublisher) PublishMessageWithSubject(topic string, message string, subject string) error {
	q, err := s.getQueueURL(topic)
	if err != nil {
		return err
	}
	_, err = s.driver.SendMessage(&sqs.SendMessageInput{
		MessageBody: &message,
		QueueUrl:    q,
		MessageAttributes: map[string]*sqs.MessageAttributeValue{
			"Subject": &sqs.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(subject),
			},
		},
	})
	return err
}

// InitTopic : create the sns topic in the system , put this in the migration path
func (s *SQSPublisher) InitTopic(topic string) (topicName string, err error) {
	panic("unsupported api call")
}

// PublishEvent : publishes the event to the topic mentioned , the event will be a json string
func (s *SQSPublisher) PublishEvent(topic string, event IEvent) error {
	return s.PublishMessage(topic, event.JSON())
}
