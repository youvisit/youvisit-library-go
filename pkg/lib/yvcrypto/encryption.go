// Package yvcrypto : encryption package that has abstractions over encryption algorithms keeps it compatible
// with DI paradigm and allows you to pick and choose algorithms.
//
//
// Usage :
// 		// for encryption with key derivation
//		enc := yvcrypto.New(yvcrypto.AES256WithKeyDerivation) // will create IEncryption interface but underlying is AES with Key derivation
// 		cipher , err := enc.Encrypt("the quick brown fox","somekey13445")
//
//		// for hashing
//		hash := yvcrypto.NewBcrypt(10) // where 10 is the cost
//		hashedPwd := hash.Hash("somePlainTextPassword")
// 		someStore.Put("user_id",hashedPwd) // this is safe now
//
package yvcrypto

const (
	// AES256WithKeyDerivation : aes 256 with pbkdf2 key derivation , use this when trying to generate a new Encryption inmplementation
	AES256WithKeyDerivation = "AES_256_W_K_DERIVATION"
)

// IEncryption : encryption interface
type IEncryption interface {
	// Encrypt : encrypt a string
	Encrypt(plainText string, key string) (string, error)
	// Decrypt : decrypt a string
	Decrypt(cipherText string, key string) (string, error)
}

// New : new encryption processor
func New(algo string) IEncryption {
	switch algo {
	case AES256WithKeyDerivation:
		return &Aes256Pbkdf2{}
	default:
		panic(interface{}("must be a supported type of encryption, use the exported variables"))
	}
}
