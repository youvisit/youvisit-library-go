// Package commander : a framework to help you make cli applications in the same style you'd use gin forexample
// it has a built in pretty printer for messages , making it dead simple to make beautiful clis
package commander

import (
	"context"
	"sync"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
	"github.com/pterm/pterm"
	"github.com/spf13/cobra"
)

// InfoBox : Info Box printer
type InfoBox struct {
	Message string
}

// TitleBox : title box printer
type TitleBox struct {
	Title      string
	Subheading string
}

// Spinner : Spinner constructor
type Spinner struct {
	Message string
}

type SuccessBox struct {
	Message string
}

// Func : commander function definition
type Func = func(ctx IContext)

//go:generate moq -skip-ensure -out commander_mock.go . IContext
// IContext : command line interface context that also has options for printing on screen
type IContext interface {
	GetArgs() []string              // get the command line arguments
	GetFlagValue(key string) string // gets the flag value as string
	GetFlagValueMap(key string) map[string]string
	GetBoolFlagValue(key string) bool                       // get boolean flag
	GetContext() context.Context                            // retrieve the context use by the application, so we can exit or chain a timer ...etc
	Exit()                                                  // exits the program successfully
	PlainOut(string)                                        // output to the console without any formatting
	Error(err *customerror.IntlErrors, withExit bool)       // output an error to the screen
	Fatal(err *customerror.IntlErrors)                      // output a fatal message and exit program
	InfoPrint(info *InfoBox)                                // information formatted context
	SuccessPrint(successMessage *SuccessBox)                // success , use this when you're about to exit
	BigTitlePrint(title *TitleBox)                          // massive title print main
	StartSpinner(spinner *Spinner)                          // starts a loading spinner
	UpdateSpinner(oldSpinner *Spinner, newSpinner *Spinner) // updates the text for the spinner
	EndSpinner(spinner *Spinner)                            // ends a loading spinner
}

func HandleCobra(fn Func) func(cmd *cobra.Command, args []string) {
	return func(cmd *cobra.Command, args []string) {
		fn(CobraContext{
			command:  cmd,
			args:     args,
			spinners: make(map[*Spinner]*pterm.SpinnerPrinter),
			mu:       sync.Mutex{},
		})
	}
}
