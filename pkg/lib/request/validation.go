package request

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"

	validation "github.com/go-ozzo/ozzo-validation"
	validationv4 "github.com/go-ozzo/ozzo-validation/v4"
)

// Validate : validates via the ozzo pacakge , just pass the strcut and rules
func Validate(input interface{}, fieldRules []*validation.FieldRules) *customerror.IntlErrors {
	err := validation.ValidateStruct(
		input,
		fieldRules...,
	)
	if err != nil {
		return customerror.New(400, err.Error())
	}
	return nil
}

// ValidateV4 : validates via the ozzo pacakge (v4) , just pass the strcut and rules
func ValidateV4(input interface{}, fieldRules []*validationv4.FieldRules) *customerror.IntlErrors {
	err := validationv4.ValidateStruct(
		input,
		fieldRules...,
	)
	if err != nil {
		return customerror.New(400, err.Error())
	}
	return nil
}

// ValidateV2 : use ozzo package without having to actually have ozzo package
func ValidateV2(input interface{}, fieldRules []*ValidationRules) *customerror.IntlErrors {
	return Validate(input, fieldRules)
}

func ValidateCli(input interface{}, fieldRules []*validation.FieldRules) *customerror.IntlErrors {
	err := validation.ValidateStruct(
		input,
		fieldRules...,
	)
	if err != nil {
		return customerror.New(1, err.Error())
	}
	return nil
}
