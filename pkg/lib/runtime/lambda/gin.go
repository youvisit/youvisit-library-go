// Package lambda : a lambda runtime helper . in here you will find http router that converts api gateway event to gin
// an sns/sqs request converter
package lambda

import (
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
)

// IsLambdaEnvironment - determines if code has been executed within lambda environment
func IsLambdaEnvironment() bool {
	return os.Getenv("LAMBDA_TASK_ROOT") != ""
}

var initialized = false
var ginLambda *ginadapter.GinLambda

// GinHandler - lambda handler proxy
func GinHandler(r *gin.Engine) func(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return func(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
		gin.SetMode
		// stdout and stderr are sent to AWS CloudWatch Logs
		log.Printf("Recieved: %s", req.Body)

		if !initialized {
			ginLambda = ginadapter.New(r)
			initialized = true
		}

		// If no name is provided in the HTTP request body, throw an error
		return ginLambda.Proxy(req)
	}
}
