package messagebroker

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/davecgh/go-spew/spew"
)

//go:generate moq  -out sns_api_mock.go . ISns
type ISns = snsiface.SNSAPI

// SnsPublisher : this implements a message pubisher
type SnsPublisher struct {
	driver       ISns
	awsAccountId string
	region       string
}

// NewSnsPublisher : creat a new publisher
func NewSnsPublisher(driver ISns, awsAccountId string, region string) *SnsPublisher {
	return &SnsPublisher{
		driver:       driver,
		awsAccountId: awsAccountId,
		region:       region,
	}
}

// ArnifyTopic : did somebody say arnie .... get to the chopper !!
// 				 not really , but this function returns an arn resource for a topic
func (s *SnsPublisher) ArnifyTopic(topic string) string {
	spew.Dump(topic)
	return fmt.Sprintf("arn:aws:sns:%s:%s:%s", s.region, s.awsAccountId, topic)
}

// SnsMockMessage : publishes a messaeg without a subject
func (s *SnsPublisher) PublishMessage(topic string, message string) error {
	spew.Dump(s.ArnifyTopic(topic))
	_, err := s.driver.Publish(&sns.PublishInput{
		Message:  &message,
		TopicArn: aws.String(s.ArnifyTopic(topic)),
	})
	spew.Dump(err)
	return err
}

// PublishMessageWithSubject : publishes a message with the subject in the header
func (s *SnsPublisher) PublishMessageWithSubject(topic string, message string, subject string) error {
	spew.Dump(s.ArnifyTopic(topic))
	_, err := s.driver.Publish(&sns.PublishInput{
		Message:  &message,
		TopicArn: aws.String(s.ArnifyTopic(topic)),
		Subject:  aws.String(subject),
	})
	spew.Dump(err)
	return err
}

// InitTopic : create the sns topic in the system , put this in the migration path
func (s *SnsPublisher) InitTopic(topic string) (topicName string, err error) {
	out, err := s.driver.CreateTopic(&sns.CreateTopicInput{
		Name: aws.String(topic),
	})
	return *out.TopicArn, err
}

// PublishEvent : publishes the event to the topic mentioned , the event will be a json string
func (s *SnsPublisher) PublishEvent(topic string, event IEvent) error {
	return s.PublishMessage(topic, event.JSON())
}

// go hack to make sure we implement the interface
var _ IMessagePublisher = &SnsPublisher{}
