// Package shell : shell command executer that escapes input
package shell

import (
	"io"
	"os"
	"os/exec"

	"github.com/spf13/afero"
)

// ExecutableSpec : executable specification for a command to run
type ExecutableSpec struct {
	BinaryCommand   string            // binary command or path to it , must not be user input !!!
	Args            []string          // arguments to augment the command , ok for user input
	EnvironmentVars map[string]string // env vars for the command before executing , keys must not be user input
	DirOverride     string            // directory to execute in
	CmdOut          io.Writer         // out for the command , default this to os.Stdout if you want it to print the stuff there , otheriwse provide a file
	BeforeExec      func()
	AfterExec       func()
}

// IExecuter : command line execute
//
//go:generate moq -skip-ensure -out shell_mock.go . IExecuter
type IExecuter interface {
	// Execute : execute a command , it will error out if there's an issue
	Execute(e *ExecutableSpec) error
	// ExecuteManySequentially : executes a bunch of commands , will fail on first one failing
	ExecuteManySequentially(eSpecs []*ExecutableSpec) error
	// GetEnv : Get an environment Variable
	Getenv(key string) string
}

// GoExec : concret impl of shell executer
type GoExec struct {
	fs afero.Fs
}

// ExecuteManySequentially : executes a bunch of commands , will fail on first one failing
func (g *GoExec) ExecuteManySequentially(eSpecs []*ExecutableSpec) error {
	for _, e := range eSpecs {
		err := g.Execute(e)
		if err != nil {
			return err
		}
	}
	return nil
}

// New : new default instance of a shell executer
func New() IExecuter {
	return &GoExec{
		fs: afero.NewOsFs(),
	}
}

// Execute : execute a command , it will error out if there's an issue
func (g *GoExec) Execute(e *ExecutableSpec) error {

	var envSlice []string
	for key, val := range e.EnvironmentVars {
		envSlice = append(envSlice, key+"="+val)
	}

	if e.BeforeExec != nil {
		e.BeforeExec()
	}

	cmd := exec.Command(e.BinaryCommand, e.Args...)
	cmd.Env = envSlice
	cmd.Dir = e.DirOverride
	out, err := cmd.CombinedOutput() // run sync
	e.CmdOut.Write(out)

	if e.AfterExec != nil {
		e.AfterExec()
	}

	return err
}

func (g *GoExec) Getenv(key string) string {
	return os.Getenv(key)
}
