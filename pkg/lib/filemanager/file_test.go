package filemanager

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/elliotchance/pie/pie"
	"github.com/stretchr/testify/assert"
)

func Test_filemanager_TimeStampifyPath(t *testing.T) {
	// path with prepend slash
	{
		filePath := "/some/file/path.jpg"
		generated := TimeStampifyPath(filePath)
		timeNow := time.Now()
		expectedPrefix := pie.Strings.Join([]string{
			"year=" + fmt.Sprint(timeNow.Year()),
			"month=" + fmt.Sprint(timeNow.Month()),
			"day=" + fmt.Sprint(timeNow.Day()),
		}, "/")
		assert.Equal(t, expectedPrefix, strings.Split(generated, filePath)[0])
	}
	// path without prepend
	{
		filePath := "some/file/path.jpg"
		generated := TimeStampifyPath(filePath)
		timeNow := time.Now()
		expectedPrefix := pie.Strings.Join([]string{
			"year=" + fmt.Sprint(timeNow.Year()),
			"month=" + fmt.Sprint(timeNow.Month()),
			"day=" + fmt.Sprint(timeNow.Day()),
		}, "/")
		assert.Equal(t, expectedPrefix, strings.Split(generated, "/"+filePath)[0])
	}
}
