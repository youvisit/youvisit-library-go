package lambda

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/messagebroker"
	"log"
	"sync"

	"github.com/aws/aws-lambda-go/events"
)

// SnsConcurrentHandler : sns aws lambda handler if your system has sns messages that need to be done in order, don't use this !
func SnsConcurrentHandler(s *messagebroker.SnsRouterHandler) func(req events.SNSEvent) {
	return func(req events.SNSEvent) {
		var wg sync.WaitGroup
		for _, ev := range req.Records {
			entity := ev.SNS
			wg.Add(1)
			// concurrently execute the sns events
			go func() {
				defer wg.Done()
				err := s.Exec(entity)
				log.Print(err) // log the error
			}()
		}
		wg.Wait()
	}
}
