package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_auth_Permissions_HasAPermission(t *testing.T) {

	permissions := Permissions{
		{
			Name:        "ahmad",
			Service:     "someService",
			Description: "someDescription",
		},
		{
			Name:        "ahmad2",
			Service:     "someService2",
			Description: "someDescription2",
		},
		{
			Name:        "ahmad24",
			Service:     "someService24",
			Description: "someDescription24",
		},
		{
			Name:        "ahmad254",
			Service:     "someService254",
			Description: "someDescripti6on24",
		},
	}
	// permission exist , but not service
	assert.False(t, permissions.HasAPermission("someServiceThatDoesn'tExists", "ahmad"))
	// service exists but not permission
	assert.False(t, permissions.HasAPermission("someService", "ahma3453td"))
	// some descritpion should not be used
	assert.False(t, permissions.HasAPermission("someDescription", "ahmad"))
	// should find 1 of the following
	assert.True(t, permissions.HasAPermission("someService", "ahmad"))
	assert.True(t, permissions.HasAPermission("someService2", "ahmad2"))
	assert.True(t, permissions.HasAPermission("someService24", "ahmad24"))
	assert.True(t, permissions.HasAPermission("someService254", "ahmad254"))
}
