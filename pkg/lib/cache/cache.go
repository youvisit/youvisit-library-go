// Package cache : abstraction over different caching solution
//
// You can Extend ICache for a different caching solution like redis
// There is an implementation for internal memory caching . feel free to add
// to this package for different services or extend the interface
//
// Usage :
//		var cache.ICache = &SomeCacheImplementation{}
//
//		type SomeCacheImplementation struct {}
//
//		func (s *SomeCacheImplementation) Get(key string, model interface{}) bool { ....}
//		func (s *SomeCacheImplementation) Set(key string, value interface{}) {...}
//		func (s *SomeCacheImplementation) SetWithExpire(key string, value interface{}, expiryTimeSeconds uint) {...}
//		func (s *SomeCacheImplementation) Delete(key string) {...}
//		func (s *SomeCacheImplementation) Flush()
package cache

//go:generate moq -skip-ensure -out cache_mock.go . ICache

// ICache : caching interface we use to set objects in the system , this can either be via an internal mem , or cluster like redis
type ICache interface {
	// Get : get a key from our persistence layer , returns a status if found
	Get(key string, model interface{}) bool
	// Set : set a key from our persistence layer ,no expiry time
	Set(key string, value interface{})
	// SetWithExpire : set a key for our persistence layer , with expiry time expressed in seconds
	SetWithExpire(key string, value interface{}, expiryTimeSeconds uint)
	// Delete : delete a key
	Delete(key string)
	// Flush : clear all cache ! ,becareful when using this
	Flush()
}
