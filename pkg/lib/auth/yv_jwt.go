package auth

import "github.com/dgrijalva/jwt-go"

// meta where instid and other stuff are stored
type Meta struct {
	InstitutionID   string `json:"inst_id"`
	UserType        string `json:"user_type"`
	AccountID       string `json:"accountid"`
	IsInternalStaff bool   `json:"is_internal_staff"`
}

// Permission : stores scope permission for user for a given service this is Modeled from
// the php server's Permissions.php Model
type Permission struct {
	Name        string `json:"name"`
	Service     string `json:"service"`
	Description string `json:"description"`
}

type Permissions []Permission

// HasAPermission : checks if the permissions passed from the php server contain a specific permission
func (p Permissions) HasAPermission(service string, permissionName string) bool {
	for i := 0; i < len(p); i++ {
		if p[i].Name == permissionName && p[i].Service == service {
			return true
		}
	}
	return false
}

// YVJwtClaim : how we expect the claim to look like
type YVJwtClaim struct {
	Meta  Meta `json:"meta"`
	Scope Permissions
	jwt.StandardClaims
}
