package yvcrypto

const (
	//BCryptHash : bcrypt hashing algoirthm
	BCryptHash = "bcrypt"
)

//go:generate moq -skip-ensure -out hasher_mock.go . IHasher

// IHasher : hasher interface you can attach to your services / repos
type IHasher interface {
	Hash(plaintText string) (hash string)
	Compare(hash string, plainText string) bool
}
