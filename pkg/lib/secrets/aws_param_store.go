package secrets

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"encoding/json"
)

// AWSParamStore : secrets fetcher using aws parameter store
type AWSParamStore struct {
	driver ssmiface.SSMAPI
	WithDecryption bool
}

// GetSecretJSON : get secret and cast json type
func (a *AWSParamStore) GetSecretJSON(secretID string, model interface{}) error {
	secretString, err := a.GetSecretString(secretID)
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(secretString), model)
}

// GetSecretString : get secret as string
func (a *AWSParamStore) GetSecretString(secretID string) (string, error) {
	out, err := a.driver.GetParameter(&ssm.GetParameterInput{
		Name : aws.String(secretID),	
	})

	if err != nil {
		return "", err
	}

	return *out.Parameter.Value, nil
}
