package http

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
	"github.com/bxcodec/faker/v3"
	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type SomeStruct struct {
	Name string
}

func initTest() (*httptest.ResponseRecorder, *GinHandler) {
	gin.SetMode("test")
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	ginHandler := GinHandler{c: c}

	return w, &ginHandler
}

func GenerateSomeRes() *SomeStruct {
	var some SomeStruct
	_ = faker.FakeData(&some)
	return &some
}

func Test_http_GinHandler_AbortWithStatusFromErrorBadRequest(t *testing.T) {
	{
		var err = errors.New("some error")
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.AbortWithStatusFromErrorBadRequest(err)
		assert.Equal(t, http.StatusBadRequest, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, err.Error(), resObj.Message)
		assert.Nil(t, resObj.Resource)
	}
}

func Test_http_GinHandler_AbortWithStatusFromIntlError(t *testing.T) {
	{
		var err = customerror.New(500, "some error")
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.AbortWithStatusFromIntlError(err)
		assert.Equal(t, 500, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, err.ErrorString(), resObj.Message)
		assert.Nil(t, resObj.Resource)
	}
}

func Test_http_GinHandler_Ok(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.Ok(someReturn)
		assert.Equal(t, http.StatusOK, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "ok", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
	}
}

func Test_http_GinHandler_Created(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.Created(someReturn)
		assert.Equal(t, http.StatusCreated, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "created record", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
	}
}

func Test_http_GinHandler_CreatedMany(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj BatchedResponse
		w, ginHandler := initTest()
		ginHandler.CreatedMany(someReturn, 200)
		assert.Equal(t, http.StatusCreated, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "created many records", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
		assert.Equal(t, int64(200), *resObj.RowsModified)
	}
}

func Test_http_GinHanlder_Updated(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.Updated(someReturn)
		assert.Equal(t, http.StatusOK, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "updated record", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
	}
}

func Test_http_GinHandler_UpdatedMany(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj BatchedResponse
		w, ginHandler := initTest()
		ginHandler.UpdatedMany(someReturn, 200)
		assert.Equal(t, http.StatusOK, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "updated many records", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
		assert.Equal(t, int64(200), *resObj.RowsModified)
	}
}

func Test_http_GinHanlder_Deleted(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj Response
		w, ginHandler := initTest()
		ginHandler.Deleted(someReturn)
		assert.Equal(t, http.StatusOK, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "deleted record", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
	}
}

func Test_http_GinHandler_DeletedMany(t *testing.T) {
	{
		someReturn := GenerateSomeRes()
		var resObj BatchedResponse
		w, ginHandler := initTest()
		ginHandler.DeletedMany(someReturn, 200)
		assert.Equal(t, http.StatusOK, w.Code)
		res, _ := ioutil.ReadAll(w.Body)
		_ = json.Unmarshal(res, &resObj)
		spew.Dump(resObj)
		// assert message is the same
		assert.Equal(t, "deleted many records", resObj.Message)
		assert.Equal(t, someReturn.Name, (resObj.Resource.(map[string]interface{}))["Name"])
		assert.Equal(t, int64(200), *resObj.RowsModified)
	}
}

func Test_http_GinHandler_IdParam(t *testing.T) {
	{

		_, ginHandler := initTest()

		ginHandler.c.Params = gin.Params{
			gin.Param{
				Key:   "id",
				Value: "123445",
			},
		}
		assert.Equal(t, "123445", ginHandler.IdParam())
	}
	// none empty str
	{
		_, ginHandler := initTest()
		assert.Equal(t, "", ginHandler.IdParam())
	}
}

func Test_http_GinHandler_Param(t *testing.T) {
	{

		_, ginHandler := initTest()

		ginHandler.c.Params = gin.Params{
			gin.Param{
				Key:   "something",
				Value: "123445",
			},
		}
		assert.Equal(t, "123445", ginHandler.Param("something"))
	}
	// none empty str
	{
		_, ginHandler := initTest()
		assert.Equal(t, "", ginHandler.Param("somethingNothere"))
	}
}

func Test_http_GinHandler_SendContext(t *testing.T) {
	{

		_, ginHandler := initTest()
		ginHandler.SendContext("something", "123")
		val, _ := ginHandler.c.Get("something")
		assert.Equal(t, "123", val)
	}
}

func Test_http_GinaHandler_GetContext(t *testing.T) {
	// if exists ok
	{

		_, ginHandler := initTest()
		ginHandler.SendContext("something", "123")
		val, _ := ginHandler.c.Get("something")
		assert.Equal(t, "123", val)

		var res string
		ginHandler.GetContext("something", &res)
		assert.Equal(t, "123", res)

	}
	// if not exist , then just don't bind
	{

		_, ginHandler := initTest()

		var res string
		ginHandler.GetContext("something", &res)
		assert.Empty(t, res)

	}
}
