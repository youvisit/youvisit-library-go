package messagebroker

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/davecgh/go-spew/spew"
)

// SqsRouterHandler : router handler for sns events
type SqsRouterHandler struct {
	mapping   map[string]EventHandlerFunc
	dlqMap    map[string]string // topic->dqlTopic
	topics    []string
	isPrfixed bool
}

// NewSqsRouterHandler : new Instance of a sqs router
func NewSqsRouterHandler(topicsArePrefixed bool) *SqsRouterHandler {
	return &SqsRouterHandler{
		mapping:   make(map[string]EventHandlerFunc),
		dlqMap:    make(map[string]string),
		isPrfixed: topicsArePrefixed,
		topics:    []string{},
	}
}

func (s *SqsRouterHandler) GetAllTopics() []string {
	return s.topics
}

func (s *SqsRouterHandler) RemapTopics(topics map[string]string) *SqsRouterHandler {
	for k, v := range topics {
		_, ok := s.mapping[k]
		if ok {
			s = s.Bind(v, s.mapping[k], s.GetDlqTopic(k))
		}
	}
	return s
}

func (s *SqsRouterHandler) MergeRouter(router *SqsRouterHandler) *SqsRouterHandler {

	for k, v := range router.mapping {
		s.mapping[k] = v
	}

	for k, v := range router.dlqMap {
		s.dlqMap[k] = v
	}

	s.topics = append(s.topics, router.topics...)

	return s
}

// Bind : bind topic to event listener
func (s *SqsRouterHandler) Bind(topicName string, handler EventHandlerFunc, dlqTopic ...string) *SqsRouterHandler {
	var dlq string
	if len(dlqTopic) > 0 {
		dlq = dlqTopic[0]
	}
	s.mapping[topicName] = handler
	s.dlqMap[topicName] = dlq
	s.topics = append(s.topics, topicName)
	return s
}

func (s *SqsRouterHandler) GetDlqTopic(regularTopic string) string {
	return s.dlqMap[regularTopic]
}

// Exec : execute router topic-> fn binding if topic name matches what's in the map
func (s *SqsRouterHandler) Exec(record events.SQSMessage, parseBody ...bool) error {
	topic := record.EventSourceARN[strings.LastIndex(record.EventSourceARN, ":")+1:]
	if len(parseBody) > 0 && parseBody[0] {
		var mp map[string]interface{}
		json.Unmarshal([]byte(record.Body), &mp)
		record.Body = mp["Message"].(string)
	}
	context := EventContextEngine{
		BodyJSON: record.Body,
		Topic:    topic,
	}
	if s.isPrfixed {
		for k, fn := range s.mapping {
			if strings.Contains(topic, k) {
				return fn(&context)
			}
		}
	} else {
		spew.Dump(s.mapping)
		spew.Dump(s.mapping[topic])
		fn := s.mapping[topic]
		if fn != nil {
			return fn(&context)
		}
	}
	return fmt.Errorf("could not find an exec for topic %s", topic)
}
