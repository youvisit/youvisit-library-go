package git

import (
	"os"

	"github.com/go-git/go-git/v5/plumbing"

	"github.com/go-git/go-git/v5"
	"golang.org/x/crypto/ssh"
	gitssh "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

var _ IGit = &GoGit{}

// GoGit : implementation of IGit interface with the go-git package
//
// this implementation is faster for smaller repos
type GoGit struct {
	ProgressLogging *os.File
}

// NewGoGit : new instance of go git
func NewGoGit(out *os.File) *GoGit {
	return &GoGit{
		ProgressLogging: out,
	}
}

// ParsePrivateKey : check that private key is valid
func (g *GoGit) ParsePrivateKey(privateKey []byte) (*SSHAuth, error) {
	return ParsePrivateKey(privateKey)
}

// CloneWithSSH : clone a repo with ssh ability
func (g *GoGit) CloneWithSSH(c *CloneOptions) error {
	auth := &gitssh.PublicKeys{Signer: c.PrivateKey.signer, User: c.SignUser}
	auth.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	cloneOpts := &git.CloneOptions{
		URL:  c.GitRepoSSHDomain,
		Auth: auth,
	}

	if c.Branch != "" {
		cloneOpts.SingleBranch = true
		cloneOpts.Depth = 1
		cloneOpts.ReferenceName = plumbing.NewBranchReferenceName(c.Branch)
	}

	_, err := git.PlainClone(c.Path, false, cloneOpts)
	if err != nil {
		return err
	}

	return nil
}
