deploy-docs-dev:
	go install golang.org/x/tools/cmd/godoc
	./document.sh

prepare-mocks:
	find . -type f -name "*mock*.go" -delete
	go generate ./...
