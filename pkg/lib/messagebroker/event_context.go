package messagebroker

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
)

var _ IEventContext = &EventContextEngine{}

type CustomErrHandlerFunc func(err *customerror.IntlErrors)

//go:generate moq -out event_context_mock.go . IEventContext

// IEventContext : event context , passed to listeners
type IEventContext interface {
	// BindBodyToEvent : bind into t a body event
	BindBodyToEvent(body interface{}) error
	// GetTopicName : Return the topic Name
	GetTopicName() string
	// HandleCustomError : Use when there;s a custom error returned
	HandleCustomError(err *customerror.IntlErrors, extraActions ...CustomErrHandlerFunc) error
}

// EventContextEngine : Concrete Impl of the Context event Interface
type EventContextEngine struct {
	BodyJSON string `json:"body"`
	Topic    string `json:"topic"`
}

// BindBodyToEvent : bind the body for the event request to a custom body
func (e *EventContextEngine) BindBodyToEvent(body interface{}) error {
	return json.Unmarshal([]byte(e.BodyJSON), body)
}

// GetTopicName : return the topic name for this event
func (e *EventContextEngine) GetTopicName() string {
	return e.Topic
}

// HandleCustomError : handle custom error from the listener controller
func (e *EventContextEngine) HandleCustomError(err *customerror.IntlErrors, extraActions ...CustomErrHandlerFunc) error {
	if err != nil {
		for _, extraAction := range extraActions {
			extraAction(err)
		}
		return fmt.Errorf("status code :%d , error Message  : %s", err.StatusCode, err.ErrorString())
	}
	return nil
}
