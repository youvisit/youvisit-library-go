package messagebroker

import (
	"encoding/json"
	"time"
)

// IEvent : describes a message broker event , it's serializable to json string
type IEvent interface {
	JSON() string // serializes the event to json string
}

// Event : describes a message broker event , that's serializable to json
type Event struct {
	DateTimeOccured  string `json:"dateTimeOccured"`  // the time it occured in MYSQL format , utc "2006-01-02 15:04:05"
	EpochTimeOccured int64  `json:"epochTimeOccured"` // the time since epoch seconds that this event occured , utc
	EventType        string `json:"eventType"`        // event type for data , ie created record , updated ..etc
	EventName        string `json:"eventName"`        // name of the event that we're sending
}

// NewEvent : generate a new event tempalte
func NewEvent(eventName string, eventType string) Event {
	t := time.Now()
	return Event{
		DateTimeOccured:  t.Format("2006-01-02 15:04:05"),
		EpochTimeOccured: t.Unix(),
		EventType:        eventType,
		EventName:        eventName,
	}
}

// JSONEV : children / composable structures can reuuse this
// func to json the struct instead of rewriting the json marshal logic
func (e *Event) JSONEV(data interface{}) string {
	bytes, _ := json.Marshal(data)
	return string(bytes)
}
