package customerror

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_customerror_New(t *testing.T) {
	// check if the errors are errors, match string , and have correctly set status code
	{
		statusCode := 400
		errStr := "hello there"

		err := New(statusCode, errStr)
		assert.Error(t, err.Error)
		assert.Equal(t, statusCode, err.StatusCode)
		assert.Equal(t, err.Error.Error(), errStr)
	}
}

func Test_customerror_ErrorString(t *testing.T) {
	// check if the errors are errors, match string , and have correctly set status code
	{
		statusCode := 400
		errStr := "hello there"

		err := New(statusCode, errStr)
		assert.Error(t, err.Error)
		assert.Equal(t, statusCode, err.StatusCode)
		assert.Equal(t, err.Error.Error(), errStr)
		assert.Equal(t, err.Error.Error(), err.ErrorString())
	}
}
