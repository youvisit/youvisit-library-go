package messagebroker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_messagebroker_Event_JSON(t *testing.T) {
	{
		ev := Event{
			Data:             "someData",
			DateTimeOccured:  "someTime",
			EpochTimeOccured: 1244,
			EventType:        "someEventType",
		}
		assert.JSONEq(t, `{"data":"someData","dateTimeOccured":"someTime","epochTimeOccured":1244,"eventType":"someEventType"}`, ev.JSON())
	}
}

func Test_messagebroker_NewEvent(t *testing.T) {
	{
		ev := NewEvent("someData", "someEventType")
		assert.Equal(t, "someData", ev.Data.(string))
		assert.Equal(t, "someEventType", ev.EventType)
		assert.NotZero(t, ev.EpochTimeOccured)
	}
}
