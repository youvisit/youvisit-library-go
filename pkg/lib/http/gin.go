package http

import (
	"bytes"
	"fmt"
	"io"
	"mime"
	"net/http"
	"path/filepath"
	"reflect"
	"time"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"

	"github.com/gin-gonic/gin"
)

// GinHandler : wraps around a gin context and implements the IContext interface contract for a
// http router
type GinHandler struct {
	c    *gin.Context
	isV2 bool
}

// HandleGin : use this to pass to the router , so you can convert domain controller function into something gin understands
func HandleGin(handler func(c IContext)) gin.HandlerFunc {
	return func(c *gin.Context) {
		g := GinHandler{c: c}
		handler(&g)
	}
}

func HandleGinV2(handler func(c IContext)) gin.HandlerFunc {
	return func(c *gin.Context) {
		g := GinHandler{c: c, isV2: true}
		handler(&g)
	}
}

func (b *GinHandler) GetCookie(cookieName string) (val string, isFound bool) {
	val, err := b.c.Cookie(cookieName)
	if err != nil {
		return "", false
	}
	return val, true
}

func (b *GinHandler) SetCookie(co *Cookie) {
	b.c.SetSameSite(co.SameSite)
	b.c.SetCookie(
		co.Name,
		co.Value,
		co.Duration,
		co.Path,
		co.Domain,
		co.Secure,
		co.HTTPOnly,
	)
}

func (b *GinHandler) DeleteCookie(cookieName string, domain string) {
	c := &http.Cookie{
		Name:     cookieName,
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		Domain:   domain,
		MaxAge:   -1,
		Secure:   true,
		HttpOnly: true,
	}
	http.SetCookie(b.c.Writer, c)
}

func (b *GinHandler) toResponse(resource interface{}, message string) interface{} {
	if b.isV2 {
		return &ResponseV2{
			Resource: resource,
			Message:  message,
		}
	}
	return &Response{
		Resource: resource,
		Message:  message,
	}
}

func (b *GinHandler) toResponseBatched(resource interface{}, message string, rowsModified *int64) interface{} {
	if b.isV2 {
		return &BatchedResponseV2{
			ResponseV2: ResponseV2{
				Resource: resource,
				Message:  message,
			},
			RowsModified: rowsModified,
		}
	}
	return &BatchedResponse{
		Response: Response{
			Resource: resource,
			Message:  message,
		},
		RowsModified: rowsModified,
	}
}

func (b *GinHandler) SetHeader(k string, v string) {
	b.c.Header(k, v)
}

func (b *GinHandler) GetRawRequest() *http.Request {
	return b.c.Request
}

func (b *GinHandler) toResponseBulkRead(resource interface{}, message string, rowsDumped *int64) interface{} {
	if b.isV2 {
		return &BulkReadResponseV2{
			ResponseV2: ResponseV2{
				Resource: resource,
				Message:  message,
			},
			RowsDumped: rowsDumped,
		}
	}
	return &BulkReadResponse{
		Response: Response{
			Resource: resource,
			Message:  message,
		},
		RowsDumped: rowsDumped,
	}
}

func (b *GinHandler) GetQueryParam(key string) string {
	return b.c.Query(key)
}

// AbortWithStatusFromErrorBadRequest : aborts with status 400 , bad request and uses error string for the message
//
// Deprecated : use AbortWithError instead and map your errors manually
func (b *GinHandler) AbortWithStatusFromErrorBadRequest(err error) {
	b.c.AbortWithStatusJSON(http.StatusBadRequest, b.toResponse(nil, err.Error()))
}

// AbortWithError : abort with an error as string and status code from the list
//
// Example:
//
//	 // call this somewhere before
//	 var SomeError = errors.New("some error with bad request")
//		http.SetErrorHandlingList(map[error]*int{
//			SomeError : http.IntPtr(400),
//		})
//
//	 // this will output status code 400 + error message as json
//	 func HandleExample (ctx http.IContext){
//			ctx.AbortWithError(SomeError)
//		}
//
//	 // this will output status code 500 + error message as json
//	 func HandleExampleGeneric (ctx http.IContext){
//			ctx.AbortWithError(errors.new("some other error"))
//		}
func (b *GinHandler) AbortWithError(err error) {
	var (
		statusCode  int
		errorString string
	)
	requestErr, isOk := err.(*RequestError)
	if isOk {
		statusCode = requestErr.StatusCode
		errorString = requestErr.Error()
	} else {
		httpStatusCode := errorToStatusCodeMap[err]
		if httpStatusCode == nil {
			statusCode = http.StatusInternalServerError
			errorString = fmt.Sprintf("Unexpected Error => %s", err.Error())
		} else {
			errorString = err.Error()
			statusCode = *httpStatusCode
		}
	}
	b.c.AbortWithStatusJSON(statusCode, b.toResponse(nil, errorString))
}

// AbortWithStatusFromIntlError : uses the intl errors http status code in the struct and the error string for the message
func (b *GinHandler) AbortWithStatusFromIntlError(err *customerror.IntlErrors) {
	b.c.AbortWithStatusJSON(err.StatusCode, b.toResponse(nil, err.ErrorString()))
}

// Ok : status 200 , ok
func (b *GinHandler) Ok(res interface{}) {
	res = modifyResponse(b, res)
	b.c.JSON(http.StatusOK, b.toResponse(res, "ok"))
}

// Accepted : accepted , will process in later time
func (b *GinHandler) Accepted(res interface{}) {
	res = modifyResponse(b, res)
	b.c.JSON(http.StatusAccepted, b.toResponse(res, "ok , accepted"))
}

// OutputFile : output file to client
func (b *GinHandler) OutputFile(file io.Reader, fileName string) {
	buf := bytes.NewBuffer(nil)
	io.Copy(buf, file)
	b.c.Header("Content-Description", "File Transfer")
	b.c.Header("Content-Disposition", "attachment; filename="+fileName)
	b.c.Header("Content-Length", fmt.Sprintf("%d", buf.Len()))
	ext := filepath.Ext(fileName)

	// Lookup the MIME type based on the file extension
	mimeType := mime.TypeByExtension(ext)

	// If the MIME type is not found, you might want to default to a generic type
	if mimeType == "" {
		mimeType = "application/octet-stream"
	}

	b.c.Data(http.StatusOK, mimeType, buf.Bytes())
}

// Created : status 201 , created record
func (b *GinHandler) Created(res interface{}) {
	res = modifyResponse(b, res)
	b.c.JSON(http.StatusCreated, b.toResponse(res, "created record"))
}

// Redirect : redirect to a url
func (b *GinHandler) Redirect(url string) {
	b.RedirectWStatus(url, http.StatusTemporaryRedirect)
}

// RedirectWStatus : redirect with a status code
func (b *GinHandler) RedirectWStatus(url string, status int) {
	b.c.Redirect(status, url)
}

// CreatedMany : status 201 , created record
func (b *GinHandler) CreatedMany(res interface{}, rows int64) {
	res = modifyResponse(b, res)
	r := b.toResponseBatched(res, "created many records", &rows)
	b.c.JSON(http.StatusCreated, r)
}

// Updated : status 200 , Updated record
func (b *GinHandler) Updated(res interface{}) {
	res = modifyResponse(b, res)
	b.c.JSON(http.StatusOK, b.toResponse(res, "updated record"))
}

func (b *GinHandler) OkManyCSV(res interface{}, rowsInCSV int64) {
	res = modifyResponse(b, res)
	r := b.toResponseBulkRead(res, "ok , many records in csv", &rowsInCSV)
	b.c.JSON(http.StatusOK, r)
}

// UpdatedMany : status 200 , updated many records , with count int64
func (b *GinHandler) UpdatedMany(res interface{}, rows int64) {
	res = modifyResponse(b, res)
	r := b.toResponseBatched(res, "updated many records", &rows)
	b.c.JSON(http.StatusOK, r)
}

// Deleted : status 200 , deleted record
func (b *GinHandler) Deleted(res interface{}) {
	res = modifyResponse(b, res)
	b.c.JSON(http.StatusOK, b.toResponse(res, "deleted record"))
}

// BindQueryWithExit : this method will set header of 400 if it can't bind query param and return false , you use the boolean to either exit the function or not
func (b *GinHandler) BindQueryWithExit(query interface{}) bool {
	err := b.c.ShouldBindQuery(query)
	if err != nil {
		b.AbortWithStatusFromErrorBadRequest(err)
		return false
	}
	return true
}

// DeletedMany : status 200 , deleted many records , with count int64
func (b *GinHandler) DeletedMany(res interface{}, rows int64) {
	res = modifyResponse(b, res)
	r := b.toResponseBatched(res, "deleted many records", &rows)
	b.c.JSON(http.StatusOK, r)
}

// BindJSONBodyWithExit : this method will set header of 400 if it can't bind body , you use the boolean to either exit the function or not
func (b *GinHandler) BindJSONBodyWithExit(body interface{}) bool {
	err := b.c.ShouldBindJSON(&body)
	if err != nil {
		b.AbortWithStatusFromErrorBadRequest(err)
		return false
	}
	return true
}

// IdParam : get param from uri with id label
func (b *GinHandler) IdParam() string {
	return b.c.Param("id")
}

// Param : get a specific param
func (b *GinHandler) Param(key string) string {
	return b.c.Param(key)
}

// Next : go to next func
func (b *GinHandler) Next() {
	b.c.Next()
}

// GetHeader : get a header from the request
func (b *GinHandler) GetHeader(key string) string {
	return b.c.GetHeader(key)
}

// SendContext : send context via gin handler
func (b *GinHandler) SendContext(key string, data interface{}) {
	b.c.Set(key, data)
}

// GetContext : get the context you sent via the SendContext method
func (b *GinHandler) GetContext(key string, dataBinding interface{}) (isFound bool) {
	val, exists := b.c.Get(key)
	// if we have the val , then bind to the expected struct
	if exists {
		v := reflect.ValueOf(dataBinding).Elem()
		v.Set(reflect.ValueOf(val))
	}
	return exists

}
