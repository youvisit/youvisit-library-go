// Package request : models a dto approach to request objects . Provides validation functionality to http requests in an idiotmatic way
package request

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
	validation "github.com/go-ozzo/ozzo-validation"
)

//go:generate moq -out request_mock.go . IRequest

// IRequest : repo input all must implement
type IRequest interface {
	// Validate : validates if input is valid otherwise errors out
	Validate() *customerror.IntlErrors
}

// ValidationRules : validation rule
type ValidationRules = validation.FieldRules
