package commander

import (
	"context"
	"os"
	"strings"
	"sync"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"
	"github.com/pterm/pterm"
	"github.com/spf13/cobra"
)

var _ IContext = &CobraContext{}

// CobraContext : Implements the ICommandContext interface
type CobraContext struct {
	command  *cobra.Command
	args     []string
	spinners map[*Spinner]*pterm.SpinnerPrinter
	mu       sync.Mutex
}

func (c CobraContext) SuccessPrint(successMessage *SuccessBox) {
	pterm.Success.Println(successMessage.Message)
}

func (c CobraContext) GetFlagValueMap(key string) map[string]string {
	res := map[string]string{}
	val, _ := c.command.Flags().GetString(key)
	valAr := strings.Split(val, ",")
	for _, k := range valAr {
		k = strings.TrimSpace(k)
		KeyValue := strings.Split(k, "=")
		if len(KeyValue) != 2 {
			continue
		}
		res[KeyValue[0]] = KeyValue[1]
	}
	return res
}

func (c CobraContext) GetFlagValue(key string) string {
	val, _ := c.command.Flags().GetString(key)
	return val
}

func (c CobraContext) GetBoolFlagValue(key string) bool {
	val, _ := c.command.Flags().GetBool(key)
	return val
}

func (c CobraContext) GetArgs() []string {
	return c.args
}

func (c CobraContext) GetContext() context.Context {
	return nil
}

func (c CobraContext) Exit() {
	os.Exit(0)
}

func (c CobraContext) PlainOut(s string) {
	pterm.Println(s)
}

func (c CobraContext) Error(err *customerror.IntlErrors, withExit bool) {
	pterm.Error.Println(err.ErrorString())
	if withExit {
		os.Exit(err.StatusCode)
	}
}

func (c CobraContext) Fatal(err *customerror.IntlErrors) {
	pterm.Fatal.Println(err.ErrorString())
	os.Exit(err.StatusCode)
}

func (c CobraContext) InfoPrint(info *InfoBox) {
	pterm.Info.Println(info.Message)
}

func (c CobraContext) BigTitlePrint(title *TitleBox) {
	s, _ := pterm.DefaultBigText.WithLetters(pterm.NewLettersFromString(title.Title)).Srender()
	pterm.DefaultCenter.Println(s)
	pterm.DefaultCenter.WithCenterEachLineSeparately().Println(title.Subheading)
}

func (c CobraContext) StartSpinner(spinner *Spinner) {
	c.mu.Lock()
	spinPterm, _ := pterm.DefaultSpinner.WithRemoveWhenDone(true).Start(spinner.Message)
	c.spinners[spinner] = spinPterm
	c.mu.Unlock()
}

func (c CobraContext) UpdateSpinner(oldSpinner *Spinner, newSpinner *Spinner) {
	c.mu.Lock()
	spinPterm := c.spinners[oldSpinner]
	spinPterm.UpdateText(newSpinner.Message)
	delete(c.spinners, oldSpinner)
	c.spinners[newSpinner] = spinPterm
	c.mu.Unlock()
}

func (c CobraContext) EndSpinner(spinner *Spinner) {
	c.mu.Lock()
	spinPterm := c.spinners[spinner]
	spinPterm.Stop()
	delete(c.spinners, spinner)
	c.mu.Unlock()
}
