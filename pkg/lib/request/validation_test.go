package request

import (
	"testing"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/assert"
)

type DummyStruct struct {
	Name        string   // must have a name
	Age         uint     // must be non zero
	LaundryList []string // must have atleast 1 item
}

func Test_handler_Validate(t *testing.T) {
	// missing all
	{
		dummy := &DummyStruct{
			Name:        "",
			Age:         uint(0),
			LaundryList: []string{},
		}
		rules := []*validation.FieldRules{
			validation.Field(&dummy.Name, validation.Required),
			validation.Field(&dummy.Age, validation.Required),
			validation.Field(&dummy.LaundryList, validation.Required),
		}
		err := Validate(dummy, rules)
		assert.NotNil(t, err)
		// the error status code
		assert.Equal(t, 400, err.StatusCode)
	}
	// missing an age
	{
		dummy := &DummyStruct{
			Name:        "something",
			Age:         uint(0),
			LaundryList: []string{},
		}
		rules := []*validation.FieldRules{
			validation.Field(&dummy.Name, validation.Required),
			validation.Field(&dummy.Age, validation.Required),
			validation.Field(&dummy.LaundryList, validation.Required),
		}
		err := Validate(dummy, rules)
		assert.NotNil(t, err)
		// the error status code
		assert.Equal(t, 400, err.StatusCode)
	}
	// missing item from laundry list
	{
		dummy := &DummyStruct{
			Name:        "something",
			Age:         uint(1),
			LaundryList: []string{},
		}
		rules := []*validation.FieldRules{
			validation.Field(&dummy.Name, validation.Required),
			validation.Field(&dummy.Age, validation.Required),
			validation.Field(&dummy.LaundryList, validation.Required),
		}
		err := Validate(dummy, rules)
		assert.NotNil(t, err)
		// the error status code
		assert.Equal(t, 400, err.StatusCode)
	}
	// follow the rules listed above
	{
		dummy := &DummyStruct{
			Name:        "something",
			Age:         uint(1),
			LaundryList: []string{"123"},
		}
		rules := []*validation.FieldRules{
			validation.Field(&dummy.Name, validation.Required),
			validation.Field(&dummy.Age, validation.Required),
			validation.Field(&dummy.LaundryList, validation.Required),
		}
		err := Validate(dummy, rules)
		assert.Nil(t, err)
	}
}
