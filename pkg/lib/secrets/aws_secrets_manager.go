package secrets

import (
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/secretsmanager/secretsmanageriface"
)

type ISecretsFace = secretsmanageriface.SecretsManagerAPI

// AWSSecretsManager : Secrets manager implements secret
type AWSSecretsManager struct {
	driver ISecretsFace
}

// GetSecretJSON : get secret and cast json type
func (a *AWSSecretsManager) GetSecretJSON(secretId string, model interface{}) error {
	secretString, err := a.GetSecretString(secretId)
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(secretString), model)
}

// GetSecretString : get secret as string
func (a *AWSSecretsManager) GetSecretString(secretId string) (string, error) {
	out, err := a.driver.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretId),
	})

	if err != nil {
		return "", err
	}

	return *out.SecretString, nil
}
