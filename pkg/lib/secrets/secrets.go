// Package secrets : provides an abstraction over different application secrets vaults
// you can use in your application and keep things testable by using the ISecrets interface and using DI
// to handle changing the concrete implementation
package secrets

//go:generate moq -skip-ensure -out secrets_mock.go . ISecrets

// ISecrets : secrets interface , implement as secrets manager , file , or something else
type ISecrets interface {
	// GetSecretJSON : get secret and cast json type
	GetSecretJSON(secretId string, model interface{}) error
	// GetSecretString : get secret as string
	GetSecretString(secretId string) (string, error)
}

func NewSecretManager(secretInterface ISecretsFace) ISecrets {
	return &AWSSecretsManager{
		driver: secretInterface,
	}
}
