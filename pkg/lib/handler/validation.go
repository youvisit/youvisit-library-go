package handler

import (
	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/customerror"

	validation "github.com/go-ozzo/ozzo-validation"
)

// Validate : validates via the ozzo pacakge , just pass the strcut and rules
func Validate(input interface{}, fieldRules []*validation.FieldRules) *customerror.IntlErrors {
	err := validation.ValidateStruct(
		input,
		fieldRules...,
	)
	if err != nil {
		return customerror.New(400, err.Error())
	}
	return nil
}
