package cache

import (
	"reflect"
	"sync"
	"time"
)

// Item : item with expiry time
type Item struct {
	resource   interface{}
	expiryTime time.Time
}

// InternalMemCache : internal memory cache , implements the ICache interface
type InternalMemCache struct {
	mu    sync.RWMutex
	items map[string]Item
}

// New : defaults creation of internal memory cache which implements the ICache interface
func New() *InternalMemCache {
	return &InternalMemCache{items: make(map[string]Item)}
}

func timeIn(secondsFromNow uint) time.Time {
	return time.Now().Add(time.Second * time.Duration(secondsFromNow))
}

// Get : get a key and set value to the interface you provided
//
// bool returned will determine if key exists or not
func (i *InternalMemCache) Get(key string, model interface{}) bool {
	// lock for reading
	i.mu.RLock()
	defer i.mu.RUnlock()
	item := i.items[key]

	// if item is expired
	if !item.expiryTime.IsZero() && item.expiryTime.Unix() < time.Now().Unix() {
		delete(i.items, key)
		return false
	}

	if item.resource != nil {
		// cast by reflection to input model
		v := reflect.ValueOf(model).Elem()
		v.Set(reflect.ValueOf(item.resource))
	}

	return item.resource != nil
}

// Set : set a key , upsert operation
func (i *InternalMemCache) Set(key string, value interface{}) {
	i.mu.Lock()
	defer i.mu.Unlock()
	i.items[key] = Item{
		expiryTime: time.Time{}, // 0 time January 1, year 1, 00:00:00.000000000 UTC.
		resource:   value,
	}
}

// SetWithExpire : set with an expiry in seconds
func (i *InternalMemCache) SetWithExpire(key string, value interface{}, expiryTimeSeconds uint) {
	i.mu.Lock()
	defer i.mu.Unlock()
	i.items[key] = Item{
		expiryTime: timeIn(expiryTimeSeconds),
		resource:   value,
	}
}

// Delete : delete item
func (i *InternalMemCache) Delete(key string) {
	i.mu.Lock()
	defer i.mu.Unlock()
	delete(i.items, key)
}

// Flush : truncate the entire cache
func (i *InternalMemCache) Flush() {
	i.mu.Lock()
	defer i.mu.Unlock()
	i.items = make(map[string]Item)
}
