package yvcrypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"strings"

	"golang.org/x/crypto/pbkdf2"
)

var (
	_ IEncryption = &Aes256Pbkdf2{}
)

// Aes256Pbkdf2 : implementation of IEncryption interface
type Aes256Pbkdf2 struct {
}

func (a *Aes256Pbkdf2) deriveKey(passphrase string, salt []byte) ([]byte, []byte) {
	if salt == nil {
		salt = make([]byte, 8)
		// http://www.ietf.org/rfc/rfc2898.txt
		// Salt.
		rand.Read(salt)
	}
	return pbkdf2.Key([]byte(passphrase), salt, 1000, 32, sha256.New), salt
}

// Encrypt : turn plain text to cipher
func (a *Aes256Pbkdf2) Encrypt(plaintext string, passphrase string) (string, error) {
	key, salt := a.deriveKey(passphrase, nil)
	iv := make([]byte, 12)
	// http://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf
	// Section 8.2
	rand.Read(iv)
	b, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCM(b)
	if err != nil {
		return "", err
	}
	data := aesgcm.Seal(nil, iv, []byte(plaintext), nil)
	return hex.EncodeToString(salt) + "-" + hex.EncodeToString(iv) + "-" + hex.EncodeToString(data), nil
}

// Decrypt : turn cipher to plain text
func (a *Aes256Pbkdf2) Decrypt(cipherText string, passphrase string) (string, error) {
	arr := strings.Split(cipherText, "-")
	salt, err := hex.DecodeString(arr[0])
	if err != nil {
		return "", err
	}
	iv, err := hex.DecodeString(arr[1])
	if err != nil {
		return "", err
	}
	data, err := hex.DecodeString(arr[2])
	if err != nil {
		return "", err
	}
	key, _ := a.deriveKey(passphrase, salt)
	b, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCM(b)
	if err != nil {
		return "", err
	}
	data, err = aesgcm.Open(nil, iv, data, nil)
	if err != nil {
		return "", err
	}
	return string(data), nil
}
