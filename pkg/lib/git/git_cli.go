package git

import (
	"fmt"
	"io"
	"os"
	"path"

	"bitbucket.org/youvisit/youvisit-library-go/pkg/lib/shell"
	"github.com/gofrs/uuid"
)

var _ IGit = &NativeCli{}

// NativeCli : implementation of the IGit interface using shell to execute git commands
//
// This will be more performant for large git repos with lots of objects . where native c code excells
type NativeCli struct {
	cmd       shell.IExecuter
	logStream io.Writer
}

// NewNativeCli : new git using shell
func NewNativeCli(out *os.File) IGit {
	return &NativeCli{
		cmd:       shell.New(),
		logStream: out,
	}
}

func (g NativeCli) ParsePrivateKey(privateKey []byte) (*SSHAuth, error) {
	return ParsePrivateKey(privateKey)
}

func (g NativeCli) CloneWithSSH(c *CloneOptions) error {
	id, _ := uuid.NewV4()
	sshKeyName := "ssh" + id.String()
	sshPath := path.Clean("/tmp" + "/" + sshKeyName)
	file, err := os.Create(sshPath)
	if err != nil {
		return err
	}
	_, _ = file.Write(c.PrivateKey.ogKey)
	file.Chmod(0600)
	file.Close()

	g.cmd.Execute(&shell.ExecutableSpec{
		BinaryCommand: "git",
		Args: []string{
			"clone",
			"--single-branch",
			"--depth",
			"1",
			"--branch",
			c.Branch,
			c.GitRepoSSHDomain,
			c.Path,
		},
		EnvironmentVars: map[string]string{
			"GIT_SSH_COMMAND": fmt.Sprintf("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i %s", sshPath),
		},
		CmdOut: g.logStream,
	})

	if err != nil {
		return err
	}

	_ = os.Remove(sshPath)
	return nil
}
