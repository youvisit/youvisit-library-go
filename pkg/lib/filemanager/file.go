package filemanager

import (
	"fmt"
	"strings"
	"time"
)

// TimeStampifyPath : takes a path string and adds a time stamp prefix
// example abe/somefile.jpg => year=2021/month=12/day=26/hour=08/abe/somefile.jpg
func TimeStampifyPath(pathSuffix string) string {
	t := time.Now()

	splitSuffix := strings.Split(pathSuffix, "/")
	if splitSuffix[0] == "" {
		splitSuffix = append(splitSuffix[:0], splitSuffix[1:]...)
	}

	return strings.Join([]string{
		"year=" + fmt.Sprint(t.Year()),
		"month=" + fmt.Sprint(t.Month()),
		"day=" + fmt.Sprint(t.Day()),
		strings.Join(splitSuffix, "/"),
	}, "/")
}
