package messagebroker

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/stretchr/testify/assert"
)

func Test_messagebroker_SnsPublisher_ArnifyTopic(t *testing.T) {
	// check if arn is correct
	{
		mock := ISnsMock{}
		sns := SnsPublisher{driver: &mock, awsAccountId: "ahmad", region: "some-region"}
		topicArn := sns.ArnifyTopic("myTopic")
		assert.Equal(t, "arn:aws:sns:some-region:ahmad:myTopic", topicArn)
	}
}

func Test_messageBorker_SnsPublisher_PublishMessage(t *testing.T) {
	mock := ISnsMock{
		PublishFunc: func(publishInput *sns.PublishInput) (*sns.PublishOutput, error) {
			assert.Equal(t, "someTopicMessage", *publishInput.Message)
			assert.Equal(t, "arn:aws:sns:some-region:ahmad:myTopic", *publishInput.TopicArn)
			return nil, nil
		},
	}
	sns := SnsPublisher{driver: &mock, awsAccountId: "ahmad", region: "some-region"}
	_ = sns.PublishMessage("myTopic", "someTopicMessage")
}

func Test_messageBorker_SnsPublisher_PublishMessageWithSubject(t *testing.T) {
	mock := ISnsMock{
		PublishFunc: func(publishInput *sns.PublishInput) (*sns.PublishOutput, error) {
			assert.Equal(t, "someTopicMessage", *publishInput.Message)
			assert.Equal(t, "someSubject", *publishInput.Subject)
			assert.Equal(t, "arn:aws:sns:some-region:ahmad:myTopic", *publishInput.TopicArn)
			return nil, nil
		},
	}
	sns := SnsPublisher{driver: &mock, awsAccountId: "ahmad", region: "some-region"}
	_ = sns.PublishMessageWithSubject("myTopic", "someTopicMessage", "someSubject")
}

func Test_messageBorker_SnsPublisher_PublishEvent(t *testing.T) {
	ev := NewEvent("someData", "someEventType")
	mock := ISnsMock{
		PublishFunc: func(publishInput *sns.PublishInput) (*sns.PublishOutput, error) {
			assert.Equal(t, ev.JSON(), *publishInput.Message)
			assert.Equal(t, "arn:aws:sns:some-region:ahmad:myTopic", *publishInput.TopicArn)
			return nil, nil
		},
	}
	sns := SnsPublisher{driver: &mock, awsAccountId: "ahmad", region: "some-region"}
	_ = sns.PublishEvent("myTopic", &ev)
}

func Test_messageBorker_SnsPublisher_InitTopic(t *testing.T) {
	mock := ISnsMock{
		CreateTopicFunc: func(createTopicInput *sns.CreateTopicInput) (*sns.CreateTopicOutput, error) {
			assert.Equal(t, "myTopic", *createTopicInput.Name)
			return &sns.CreateTopicOutput{
				TopicArn: aws.String("someArn"),
			}, nil
		},
	}
	sns := SnsPublisher{driver: &mock, awsAccountId: "ahmad", region: "some-region"}
	topicName, _ := sns.InitTopic("myTopic")
	assert.Equal(t, "someArn", topicName)
}
