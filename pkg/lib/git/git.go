// Package git : abstract interface that wraps around git , this exposes methods related to git
// like clone ..etc
package git

import "golang.org/x/crypto/ssh"

// SSHAuth : ssh authorization return
type SSHAuth struct {
	signer ssh.Signer
	ogKey  []byte
}

// CloneOptions : Cloning options
type CloneOptions struct {
	Path             string
	GitRepoSSHDomain string
	PrivateKey       SSHAuth
	SignUser         string
	Branch           string // branch is optional
}

//go:generate moq -skip-ensure -out git_mock.go . IGit

// IGit : git interface makes it easier to pull things with auth
type IGit interface {
	// ParsePrivateKey : check that privateKey is Valid
	ParsePrivateKey(privateKey []byte) (*SSHAuth, error)
	// CloneWithSSH : clone a repo using ssh as auth method
	CloneWithSSH(c *CloneOptions) error
}

// ParsePrivateKey : parse private key passed as bytes
func ParsePrivateKey(privateKey []byte) (*SSHAuth, error) {
	sshKey, err := ssh.ParsePrivateKey([]byte(privateKey))
	if err != nil {
		return nil, err
	}
	return &SSHAuth{
		signer: sshKey,
		ogKey:  privateKey,
	}, nil
}
