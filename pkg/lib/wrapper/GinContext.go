package wrapper

// Mock Context Interface
//go:generate moq -skip-ensure -pkg mocks -out mocks/gin_context_mock.go . IGinContext
type IGinContext interface {
	// Query : query string parse
	Query(queryParam string) string
	// returns full path
	FullPath() string
	// AbortWithStatusJSON calls `Abort()` and then `JSON` internally.
	// This method stops the chain, writes the status code and return a JSON body.
	// It also sets the Content-Type as "application/json".
	AbortWithStatusJSON(code int, jsonObj interface{})
	// Param returns the value of the URL param.
	// It is a shortcut for c.Params.ByName(key)
	//     router.GET("/user/:id", func(c *gin.Context) {
	//         // a GET request to /user/john
	//         id := c.Param("id") // id == "john"
	//     })
	Param(key string) string
	// DefaultQuery returns the keyed url query value if it exists,
	// otherwise it returns the specified defaultValue string.
	// See: Query() and GetQuery() for further information.
	//     GET /?name=Manu&lastname=
	//     c.DefaultQuery("name", "unknown") == "Manu"
	//     c.DefaultQuery("id", "none") == "none"
	//     c.DefaultQuery("lastname", "none") == ""

	// ShouldBind checks the Content-Type to select a binding engine automatically,
	// Depending the "Content-Type" header different bindings are used:
	//     "application/json" --> JSON binding
	//     "application/xml"  --> XML binding
	// otherwise --> returns an error
	// It parses the request's body as JSON if Content-Type == "application/json" using JSON or XML as a JSON input.
	// It decodes the json payload into the struct specified as a pointer.
	// Like c.Bind() but this method does not set the response status code to 400 and abort if the json is not valid.
	ShouldBind(obj interface{}) error
	// ShouldBindJSON is a shortcut for c.ShouldBindWith(obj, binding.JSON).
	ShouldBindJSON(obj interface{}) error
	// ShouldBindQuery is a shortcut for c.ShouldBindWith(obj, binding.Query).
	ShouldBindQuery(obj interface{}) error
	// JSON serializes the given struct as JSON into the response body.
	// It also sets the Content-Type as "application/json".
	JSON(code int, obj interface{})
	// gets a header key
	GetHeader(headerKey string) string
	// used inside middleware
	Next()
	// Adds a header to response
	Header(key string, value string)
}
