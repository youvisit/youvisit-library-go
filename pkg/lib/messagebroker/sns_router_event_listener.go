package messagebroker

import (
	"errors"
	"fmt"
	"strings"
	"sync"

	"github.com/aws/aws-lambda-go/events"
)

var (
	errorNoBindingForThisEvent = errors.New("unexpected event , there is no routing for this event")
)

// EventHandlerFunc : event handler generic function
type EventHandlerFunc = func(ev IEventContext) error

// SqsRouterHandler : router handler for sns events
type SnsRouterHandler struct {
	mapping map[string]EventHandlerFunc
	mu      sync.Mutex
}

// NewSnsRouterHandler : new Instance of an sns router
func NewSnsRouterHandler() *SnsRouterHandler {
	return &SnsRouterHandler{
		mapping: make(map[string]EventHandlerFunc),
		mu:      sync.Mutex{},
	}
}

// Bind : bind topic to event listener
func (s *SnsRouterHandler) Bind(topicName string, handler EventHandlerFunc) {
	s.mu.Lock()
	s.mapping[topicName] = handler
	s.mu.Unlock()
}

// Exec : execute router topic-> fn binding if topic name matches what's in the map
func (s *SnsRouterHandler) Exec(record events.SNSEntity) error {
	topic := record.TopicArn[strings.LastIndex(record.TopicArn, ":")+1:]
	fn := s.mapping[topic]
	if fn != nil {
		context := EventContextEngine{
			BodyJSON: record.Message,
			Topic:    topic,
		}
		return fn(&context)
	}
	return fmt.Errorf("%w  : %s", errorNoBindingForThisEvent, topic)
}
